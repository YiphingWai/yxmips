module yxmips
(
    output reg[15:0]led,
    output reg[1:0]ledrg0,
    output reg[1:0]ledrg1,
    output reg[7:0]wela,
    output reg[7:0]dula,
    
    input[3:0]key,
    
    input clk_i,
    input rst_n
);


wire clk;

wire[31:0] instaddr;
wire[31:0] inst_rom;

wire ram_en;
wire ram_we;
wire[3:0] ram_byte_vld;
wire[31:0] ram_addr;
wire[31:0] ram_data_w;
wire[31:0] ram_data_r;
wire[31:0] data_r = (ram_addr[31:16] == 16'hbfd0) ? reg_data_r : ram_data_r;
yx_h32 core
(
    .instaddr(instaddr),
    .instdata(inst_rom),
    
    .ram_en(ram_en),
    .ram_we(ram_we),
    .ram_byte_vld(ram_byte_vld),
    .ram_addr(ram_addr),
    .ram_data_o(ram_data_w),
    .ram_data_i(data_r),
    
    .int_i(5'b0),   //��Ӧ�жϺ�5-1
    
    .clk(clk),
    .rst_n(rst_n)
);



wire cache_clk;

pll pll0
 (
  // Clock out ports
  .clk_out1(clk),
  .clk_out2(cache_clk),
 // Clock in ports
  .clk_in1(clk_i)
 );


rom rom0
(
    .clka(cache_clk),
    .ena(1'b1),
    .addra(instaddr[31:2]),
    .douta(inst_rom)
);

ram ram0
(
    .clka(cache_clk),
    .ena(ram_en && (ram_addr[31:16] != 16'hbfd0)),
    .wea(ram_we?ram_byte_vld:4'd0),
    .addra(ram_addr[31:2]),
    .dina (ram_data_w),
    .douta(ram_data_r)
);

reg[31:0] reg_data_r;
reg[31:0] digit;
always @(posedge cache_clk,negedge rst_n)
begin
    if(!rst_n)
    begin
        led = 16'h0000;
        ledrg0 = 2'd0;
        ledrg1 = 2'd0;
        digit = 32'd0;
    end
    else
        if(ram_addr[31:16]==16'hbfd0)
        begin
            case(ram_addr[15:0])
            
            16'hf000:
                if(ram_we)
                    led = ram_data_w[15:0];
            16'hf004:
                if(ram_we)
                    ledrg0 = ram_data_w[1:0];
            16'hf008:
                if(ram_we)
                    ledrg1 = ram_data_w[1:0];
            16'hf010:
                if(ram_we)
                    digit = ram_data_w;
            16'hf024:
                reg_data_r = {16'b0, key, 12'b0};
            
            endcase
        end
end

reg[18:0] cnt;
always @(posedge clk, negedge rst_n)
    if(!rst_n)
        cnt <= 19'd0;
    else if(cnt == 19'd29999)
        cnt <= 19'd0;
    else
        cnt <= cnt + 19'd1;

wire clk_digit = (cnt == 19'd29999);

function [7:0] MAP; 
    input[3:0]num; 
    
    begin 
        case(num)
        4'd0:
            MAP = 8'h3f;
        4'd1:
            MAP = 8'h06;
        4'd2:
            MAP = 8'h5b;
        4'd3:
            MAP = 8'h4f;
        4'd4:
            MAP = 8'h66;
        4'd5:
            MAP = 8'h6d;
        4'd6:
            MAP = 8'h7d;
        4'd7:
            MAP = 8'h07;
        4'd8:
            MAP = 8'h7f;
        4'd9:
            MAP = 8'h6f;
        4'd10:
            MAP = 8'h77;
        4'd11:
            MAP = 8'h7c;
        4'd12:
            MAP = 8'h39;
        4'd13:
            MAP = 8'h5e;
        4'd14:
            MAP = 8'h79;
        default:
            MAP = 8'h71;
       
       endcase
    end 
endfunction


always @(posedge clk_digit, negedge rst_n)
    if(!rst_n)
    begin
        wela <= 8'b11111110;
        dula <= MAP(digit[3:0]);
    end
    else
        case(wela)
        
        8'b11111110:
        begin
            wela <= 8'b11111101;
            dula <= MAP(digit[7:4]);
        end  
         
        8'b11111101:
        begin
            wela <= 8'b11111011;
            dula <= MAP(digit[11:8]);
        end 
        
        8'b11111011:
        begin
            wela <= 8'b11110111;
            dula <= MAP(digit[15:12]);
        end
        
        8'b11110111:
        begin
            wela <= 8'b11101111;
            dula <= MAP(digit[19:16]);
        end
        
        8'b11101111:
        begin
            wela <= 8'b11011111;
            dula <= MAP(digit[23:20]);
        end
        
        8'b11011111:
        begin
            wela <= 8'b10111111;
            dula <= MAP(digit[27:24]);
        end
        
        8'b10111111:
        begin
            wela <= 8'b01111111;
            dula <= MAP(digit[31:28]);
        end
        
        8'b01111111:
        begin
            wela <= 8'b11111110;
            dula <= MAP(digit[3:0]);
        end
/*        
        default:
        begin
            wela <= 8'hff;
            dula <= 8'hff;
        end*/
        endcase

endmodule
