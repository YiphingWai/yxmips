module div(

	input clk,  //时钟信号
	input rst_n, //主复位信号
	
	input signed_div_i, //有无符号
	input[31:0] opdata1_i, //操作数1
	input[31:0] opdata2_i, //操作数2
	input start_i, //开始信号
	input clr_i, //清除信号
	
	output reg[63:0] result_o, //结果
	output reg stall_o //总线阻塞信号
);

	wire[32:0] div_temp; 
	reg[5:0] cnt; //计数器
	reg[64:0] dividend; //试除中间结果
	reg[1:0] state; //状态
	reg[31:0] divisor;	//被除数
	reg[31:0] temp_op1; //临时操作数1
	reg[31:0] temp_op2; //临时操作数2
	reg sgn_fix1;
	reg sgn_fix2;
	
	assign div_temp = {1'b0,dividend[63:32]} - {1'b0,divisor};

	always @ (posedge clk, negedge rst_n) 
	begin
		if (!rst_n) 
		begin
			state <= 2'd0;
		end 
		else 
		begin
		  case (state)
		  	2'd0:			
			begin               //DivFree״̬
		  		if(start_i == 1'b1 && clr_i == 1'b0)  //允许除
				begin
					
		  			if(opdata2_i == 32'd0) //0作除数触发异常
					begin
		  				state <= 2'd1;
		  			end 
					else //正常情况
					begin
		  				state <= 2'd2;
		  				cnt <= 6'b000000;
		  				if(signed_div_i == 1'b1 && opdata1_i[31] == 1'b1 )  //处理第1个立即数为负
						begin
		  					temp_op1 = ~opdata1_i + 1;
		  					sgn_fix1 = 1;
		  				end 
						else 
						begin
		  					temp_op1 = opdata1_i;
		  					sgn_fix1 = 0;
		  				end
		  				if(signed_div_i == 1'b1 && opdata2_i[31] == 1'b1 )  //处理第2个立即数为负
						begin
		  					temp_op2 = ~opdata2_i + 1;
		  					sgn_fix2 = 1;
		  				end 
						else 
						begin
		  					temp_op2 = opdata2_i;
		  					sgn_fix2 = 0;
		  				end
						dividend <= {32'd0,32'd0};
						dividend[32:1] <= temp_op1; //商初值
						divisor <= temp_op2;
					end
				end        	
		  	end
		  	2'd1:		
			begin               //DivByZero״̬
         		dividend <= {32'd0,32'd0};
				state <= 2'd3;		 		
		  	end
		  	2'd2:				
			begin               //DivOn״̬
		  		if(clr_i == 1'b0) 
				begin
		  			if(cnt != 6'b100000) 
					begin
						if(div_temp[32] == 1'b1) //试除
						begin
							dividend <= {dividend[63:0] , 1'b0}; //除不尽商自然移位，余数移位
						end 
						else 
						begin
							dividend <= {div_temp[31:0] , dividend[31:0] , 1'b1}; //除得尽商移位补1，余数更新
               			end
               			cnt <= cnt + 1;
             		end 
					else 
					begin
                		if((signed_div_i == 1'b1) && ((sgn_fix1 ^ sgn_fix2) == 1'b1)) //恢复商
						begin
	                    	dividend[31:0] <= (~dividend[31:0] + 1);
    	           		end
        	       		if((signed_div_i == 1'b1) && ((sgn_fix1 ^ dividend[64]) == 1'b1)) //恢复余数
						begin              
                	  		dividend[64:33] <= (~dividend[64:33] + 1);
               			end
               			state <= 2'd3;
               			cnt <= 6'b000000;            	
             		end
		  		end 
				else 
				begin
		  			state <= 2'd0;
		  		end	
		  	end
		  	2'd3:			//DivEnd״̬
			if(start_i == 1'b0 || clr_i == 1'b1)
	         state <= 2'd0;

		  endcase
		end
	end

	always @(*)
	begin
		if(!rst_n) 
		begin
			stall_o = 1'b0;
			result_o <= {32'd0,32'd0}; 
		end
		else
			case(state)
			2'd0:
				if(start_i == 1'b1 && clr_i == 1'b0) 
				begin
					stall_o = 1'b1;
					result_o = {32'd0,32'd0}; 
				end
				else
				begin
					stall_o = 1'b0;
					result_o = {32'd0,32'd0}; 
				end
			2'd1:
			begin
				stall_o = 1'b1;
				result_o = {32'd0,32'd0}; 
			end
			2'd2:
			begin
				stall_o = 1'b1;
				result_o = {32'd0,32'd0}; 
			end
			2'd3:
			begin
				result_o = {dividend[64:33], dividend[31:0]};  //余数->hi 商->lo
				stall_o = 1'b0;
			end
			
			endcase
	end
	
endmodule
