`include "./defines.v"

module yx_h32
(
    output[31:0] instaddr,
    input[31:0] instdata,
    
    output ram_en,
    output ram_we,
    output[3:0] ram_byte_vld,
    output[31:0] ram_addr,
    output[31:0] ram_data_o,
    input[31:0] ram_data_i,
    
	input[4:0] int_i,	//对应中断号5-1
	
	input clk,
	input rst_n
);


assign ram_en = (load_type_M != 4'd0) || (str_type_M != 4'd0);
assign ram_byte_vld = byte_vld_M;
assign ram_addr = aluout_M;
assign ram_data_o = mod_data_M;

wire[31:0] rdata_ram = ram_data_i;

assign ram_we = memwe_M && !store_exc_M;



//******************************************
wire[31:0] pc_plus4_F;
wire is_bj_inst_F;
wire[31:0] instaddr;
wire[31:0] instdata;
wire[31:0] inst_rom;
wire[31:0] inst_cache;
wire[7:0] fetch_exc_F;
//******************************************
wire pc_sel_D;
wire[31:0] ir_D;
wire[31:0] pc_plus4_D;
wire[7:0]  fetch_exc_D;

wire is_div_D;
wire is_sign_div_D;
wire cp0_wen_D;
wire[3:0] str_type_D;
wire[3:0] load_type_D;
wire hisrc_sel_D;
wire losrc_sel_D;
wire hiwe_D;
wire lowe_D;
wire regwe_D;
wire[1:0] rlt_sel_D;
wire memwe_D;
//wire branch_en_D;
wire sign_D;
wire[7:0] alucode_D;
wire alusrc0_sel_D;
wire[1:0] alusrc1_sel_D;
wire[1:0] regdst_sel_D;

wire[31:0] rd0_D;
wire[31:0] rd1_D;

wire[31:0] hi_D;
wire[31:0] lo_D;

wire[31:0] cp0_D;

wire[31:0] signimm_D;
wire[31:0] pc_branch_D;

wire[31:0] jrd0_D;
wire[31:0] jrd1_D;

wire is_bj_inst_reg_D;
wire is_bj_inst_D;
wire is_delayslot_D;
wire[7:0] exc_mask_D;
//******************************************
wire is_div_E;
wire is_sign_div_E;
wire cp0_wen_E;
wire[3:0] str_type_E;
wire[3:0] load_type_E;
wire hisrc_sel_E;
wire losrc_sel_E;
wire hiwe_E;
wire lowe_E;
wire regwe_E;
wire[1:0] rlt_sel_E;
wire memwe_E;
wire[7:0] alucode_E;
wire alusrc0_sel_E;
wire[1:0] alusrc1_sel_E;
wire[1:0] regdst_sel_E;
wire[7:0] fetch_exc_E;
	
wire[31:0] rd0_E;
wire[31:0] rd1_E;

	
wire[31:0] hi_E;
wire[31:0] lo_E;

wire[31:0] cp0_E;

wire[4:0] rs_E;
wire[4:0] rt_E;
wire[4:0] rd_E;
	
wire[31:0] signimm_E;
	
wire[31:0] src0_E;
wire[31:0] src1_E;

wire[31:0] regdat0_E;
wire[31:0] regdat1_E;
wire[31:0] aluout_E;
wire alu_we_E;
wire alu_mwe_E;
wire[63:0] mulrlt_E;
wire[63:0] divrlt_E;
wire[3:0] byte_vld_E;
wire[4:0] regdst_E;

wire[31:0] pc_plus4_E;
wire[31:0] pc_plus8_E;

wire[31:0] fw_rd0_E;
wire[31:0] fw_rd1_E;
wire[31:0] fw_hi_E;
wire[31:0] fw_lo_E;
wire fw_ll_bit_E;
wire[31:0] fw_cp0_E;

wire is_delayslot_E;
wire[7:0] exc_mask_E;
wire[7:0] alu_exc_E;
//******************************************
wire load_exc_M;
wire store_exc_M;
wire cp0_wen_M;
wire[3:0] str_type_M;
wire[3:0] load_type_M;


wire hisrc_sel_M;
wire losrc_sel_M;
wire hiwe_M;
wire lowe_M;
wire regwe_M;
wire[1:0] rlt_sel_M;
wire memwe_M;

wire sc_rlt_M;
wire[31:0] regdat0_M;
wire[31:0] regdat1_M;
wire[31:0] mod_data_M;
wire[31:0] read_data_M;
wire[31:0] aluout_M;
wire[3:0] byte_vld_M;
wire[63:0] mulrlt_M;
	
wire[4:0] regdst_M;

wire[31:0] pc_plus4_M;

wire is_delayslot_M;

wire ll_bit_M;
wire[31:0] hi_M;
wire[31:0] lo_M;
wire[4:0] int_M;
wire[7:0] exc_mask_M;
//******************************************
wire cp0_wen_W;
wire[3:0] load_type_W;
wire[4:0] regdst_W;
wire[31:0] regdat1_W;
wire[31:0] rlt_W;
wire regwe_W;

wire[31:0] hi_W;
wire[31:0] lo_W;
wire hiwe_W;
wire lowe_W;

wire hisrc_sel_W;
wire losrc_sel_W;
wire[1:0] rlt_sel_W;
		
wire sc_rlt_W;
wire[31:0] read_data_W;
wire[31:0] sign_data_W;
wire[31:0] aluout_W;
wire[63:0] mulrlt_W;
wire[3:0] byte_vld_W;

wire ll_bit_W;
wire llbit_set_W;

//******************************************
wire[1:0] fw_rd0_sel;
wire[1:0] fw_rd1_sel;
wire[1:0] fw_hi_sel;
wire[1:0] fw_lo_sel;
wire stcl_lw;
wire[1:0] fw_jrd0_sel;
wire[1:0] fw_jrd1_sel;
wire[1:0] fw_cp0_sel;
wire stcl_jmp;
wire exc_en;
wire[31:0] exc_addr;
wire stcl_f = 1'b0;
wire stcl_ram_cache;
wire stcl_rom;
wire stcl_icache;
wire stcl_div;

wire[31:0] vic_addr;
wire vic_is_delayslot;

pc pc0
(
	//常规跳转接口
	.pc_load_i(pc_branch_D),
	.pc_sel_i(pc_sel_D),
	//中断接口（优先）
	.pc_sel_exc_i(exc_en),
	.pc_load_exc_i(exc_addr),
	
	.pc_o(instaddr),
	.pc_plus4_o(pc_plus4_F),
	.fetch_exc_o(fetch_exc_F),
	
	.stall0_i(stcl_lw),
	.stall1_i(stcl_jmp),
	.stall2_i(stcl_f),
	.stall3_i(stcl_ram_cache | stcl_div),
	.rst_n(rst_n),
	.clk(clk)
);

ir ir0
(
	.ir_i(instdata),
	.pc_plus4_i(pc_plus4_F),
	.is_bj_inst_i(is_bj_inst_F),
	.fetch_exc_i(fetch_exc_F),
	
	.ir_o(ir_D),
	.pc_plus4_o(pc_plus4_D),
	.is_bj_inst_o(is_bj_inst_D),
	.fetch_exc_o(fetch_exc_D),
	
	.stall0_i(stcl_lw),
	.stall1_i(stcl_jmp),
	.stall2_i(stcl_f),
	.stall3_i(stcl_ram_cache | stcl_div),
	.irq_i(exc_en),
	.rst_n(rst_n),
	.clk(clk)
);

mux_31 fw_jrd0_mux
(
	.in0_i(rd0_D),
	.in1_i(aluout_M),
	.in2_i(rlt_W),

	.sel_i(fw_jrd0_sel),
	
	.out_o(jrd0_D)
);

mux_31 fw_jrd1_mux
(
	.in0_i(rd1_D),
	.in1_i(aluout_M),
	.in2_i(rlt_W),

	.sel_i(fw_jrd1_sel),
	
	.out_o(jrd1_D)
);
ctrl ctrl0
(
	.inst_i(ir_D),

	.rd0_i(jrd0_D),
	.rd1_i(jrd1_D),
	.pc_plus4_i(pc_plus4_D),
	.is_bj_inst_i(is_bj_inst_D),
	
	.is_div_o(is_div_D),
	.is_sign_div_o(is_sign_div_D),
	.exc_mask_o(exc_mask_D),
	.is_delayslot_o(is_delayslot_D),
	.cp0_wen_o(cp0_wen_D),
	.str_type_o(str_type_D),
	.load_type_o(load_type_D),
	.hisrc_sel_o(hisrc_sel_D),
	.losrc_sel_o(losrc_sel_D),
	.hiwe_o(hiwe_D),
	.lowe_o(lowe_D),
	.regwe_o(regwe_D),
	.rlt_sel_o(rlt_sel_D),
	.memwe_o(memwe_D),
	.alucode_o(alucode_D),
	.sign_o(sign_D),
	.alusrc0_sel_o(alusrc0_sel_D),
	.alusrc1_sel_o(alusrc1_sel_D),
	.regdst_sel_o(regdst_sel_D),
	
	.is_bj_inst_reg_o(is_bj_inst_reg_D),
	.pc_sel_o(pc_sel_D),
	//.branch_en_o(branch_en_D),
	.pc_branch_o(pc_branch_D),
	
	.is_bj_inst_o(is_bj_inst_F),
	
	.rst_n(rst_n)
);

regfile regfile0
(
	.ra0_i(ir_D[25:21]),
	.ra1_i(ir_D[20:16]),
	
	.wa0_i(regdst_W),
	.wd0_i(rlt_W),
	.we0(regwe_W),
	
	.rd0_o(rd0_D),
	.rd1_o(rd1_D),
	
	.rst_n(rst_n),
	.clk(clk)
);

hilo hilo0
(
	.hi_i(hi_W),
	.lo_i(lo_W),
	.hi_we(hiwe_W),
	.lo_we(lowe_W),
	
	.hi_o(hi_D),
	.lo_o(lo_D),
	
	.clk(clk),
	.rst_n(rst_n)
);

assign load_exc_M = ((load_type_M==`LOAD_LH || load_type_M==`LOAD_LHU)&&ram_addr[0])||
					((load_type_M==`LOAD_LL || load_type_M==`LOAD_LW)&&ram_addr[1:0]!=2'd0);
assign store_exc_M = (str_type_M==`STR_SH && ram_addr[0])||
					 ((str_type_M==`STR_SW || str_type_M==`STR_SC)&&ram_addr[1:0]!=2'd0);

cp0 cp00
(
	.we_i(cp0_wen_W),
	.wa_i(regdst_W),
	.wd_i(rlt_W),
	.ra_i(ir_D[15:11]),
	
	//中断触发，M级信号
	.except_type_i(exc_mask_M|{store_exc_M,load_exc_M,6'd0}),
	.int_i(int_M),		//外部中断
	//保存现场
	.vic_inst_addr_i(vic_addr),
	.is_in_delayslot_i(vic_is_delayslot),
	.badvaddr_i(ram_addr),
	
	.data_o(cp0_D),
	.count_o(),
	.compare_o(),
	.status_o(),
	.cause_o(),
	.epc_o(),
	.config_o(),
	.prid_o(),
	
	.exc_en_o(exc_en),
	.exc_addr_o(exc_addr),
	
	.clk(clk),
	.rst_n(rst_n)
);





signimm signimm0
(
	.imm_i(ir_D[15:0]),
	.sign_i(sign_D),
	
	.signimm_o(signimm_D)
);



syncD2E syncD2E0
(
	.is_div_i(is_div_D),
	.is_sign_div_i(is_sign_div_D),
	.exc_mask_i(exc_mask_D),
	.is_delayslot_i(is_delayslot_D),
	.cp0_wen_i(cp0_wen_D),
	.str_type_i(str_type_D),
	.load_type_i(load_type_D),
	.hisrc_sel_i(hisrc_sel_D),
	.losrc_sel_i(losrc_sel_D),
	.hiwe_i(hiwe_D),
	.lowe_i(lowe_D),
	.regwe_i(regwe_D),
	.rlt_sel_i(rlt_sel_D),
	.memwe_i(memwe_D),
	.alucode_i(alucode_D),
	.alusrc0_sel_i(alusrc0_sel_D),
	.alusrc1_sel_i(alusrc1_sel_D),
	.regdst_sel_i(regdst_sel_D),
	
	.rd0_i(rd0_D),
	.rd1_i(rd1_D),
	
	.hi_i(hi_D),
	.lo_i(lo_D),
	
	.cp0_i(cp0_D),
	
	.rs_i(ir_D[25:21]),
	.rt_i(ir_D[20:16]),
	.rd_i(ir_D[15:11]),
	
	.signimm_i(signimm_D),
	.pc_plus4_i(pc_plus4_D),
	.fetch_exc_i(fetch_exc_D),
//--------------------------------------
	.is_div_o(is_div_E),
	.is_sign_div_o(is_sign_div_E),
	.exc_mask_o(exc_mask_E),
	.is_delayslot_o(is_delayslot_E),
	.cp0_wen_o(cp0_wen_E),
	.str_type_o(str_type_E),
	.load_type_o(load_type_E),
	.hisrc_sel_o(hisrc_sel_E),
	.losrc_sel_o(losrc_sel_E),
	.hiwe_o(hiwe_E),
	.lowe_o(lowe_E),
	.regwe_o(regwe_E),
	.rlt_sel_o(rlt_sel_E),
	.memwe_o(memwe_E),
	.alucode_o(alucode_E),
	.alusrc0_sel_o(alusrc0_sel_E),
	.alusrc1_sel_o(alusrc1_sel_E),
	.regdst_sel_o(regdst_sel_E),
	
	
	.rd0_o(rd0_E),
	.rd1_o(rd1_E),
	
	.hi_o(hi_E),
	.lo_o(lo_E),
	
	.cp0_o(cp0_E),
	
	.rs_o(rs_E),
	.rt_o(rt_E),
	.rd_o(rd_E),
	
	.signimm_o(signimm_E),
	
	.pc_plus4_o(pc_plus4_E),
	.fetch_exc_o(fetch_exc_E),
	
	.irq_i(exc_en),
	.clr0_i(stcl_lw),
	.clr1_i(stcl_jmp),
	.clr2_i(stcl_f),
	.stall0_i(stcl_ram_cache | stcl_div),
	.rst_n(rst_n),
	.clk(clk)
);

mux_31 fw_rd0_mux_E
(
	.in0_i(rd0_E),
	.in1_i(aluout_M),
	.in2_i(rlt_W),
	
	.sel_i(fw_rd0_sel),
	
	.out_o(fw_rd0_E)
);

mux_31 fw_rd1_mux_E
(
	.in0_i(rd1_E),
	.in1_i(aluout_M),
	.in2_i(rlt_W),
	
	.sel_i(fw_rd1_sel),
	
	.out_o(fw_rd1_E)
);

mux_31 fw_hi_mux_E
(
	.in0_i(hi_E),
	.in1_i(hi_M),
	.in2_i(hi_W),
	
	.sel_i(fw_hi_sel),
	
	.out_o(fw_hi_E)
);

mux_31 fw_lo_mux_E
(
	.in0_i(lo_E),
	.in1_i(lo_M),
	.in2_i(lo_W),
	
	.sel_i(fw_lo_sel),
	
	.out_o(fw_lo_E)
);


assign regdat1_E = fw_rd1_E;

mux_21 src0_mux
(
	.in0_i(fw_rd0_E),
	.in1_i(signimm_E),
	
	.sel_i(alusrc0_sel_E),
	
	.out_o(src0_E)
); 

mux_41 src1_mux
(
	.in0_i(fw_rd1_E),
	.in1_i(fw_hi_E),
	.in2_i(fw_lo_E),
	.in3_i(signimm_E),
	
	.sel_i(alusrc1_sel_E),
	
	.out_o(src1_E)
); 

mux_21 llbit_mux
(
	.in0_i(ll_bit_W),
	.in1_i(ll_bit_M),
	
	.sel_i(ll_bit_M),
	
	.out_o(fw_ll_bit_E)
);


mux_31 cp0_mux
(
	.in0_i(cp0_E),
	.in1_i(aluout_M),
	.in2_i(rlt_W),
	
	.sel_i(fw_cp0_sel),
	
	.out_o(fw_cp0_E)
);


assign pc_plus8_E = pc_plus4_E + 32'd4;
alu alu0
(
	.alucode_i(alucode_E),
	.src0_i(src0_E),
	.src1_i(src1_E),
	.ll_bit_i(fw_ll_bit_E),
	.hilo_i({fw_hi_E, fw_lo_E}),
	.pc_plus8_i(pc_plus8_E),
	.cp0_i(fw_cp0_E),
	
	.aluout_o(aluout_E),
	.alu_we_o(alu_we_E),
	.alu_mwe_o(alu_mwe_E),
	.mulrlt_o(mulrlt_E),
	.byte_vld_o(byte_vld_E),
	.alu_exc_o(alu_exc_E)
);

div div0
(

	.clk(clk),
	.rst_n(rst_n),
	
	.signed_div_i(is_sign_div_E),
	.opdata1_i(src0_E),
	.opdata2_i(src1_E),
	.start_i(is_div_E),
	.clr_i(1'b0),
	
	.result_o(divrlt_E),
	.stall_o(stcl_div)
);





mux5b_31 regdst_mux
(
	.in0_i(rt_E),
	.in1_i(rd_E),
	.in2_i(5'd31),
	
	.sel_i(regdst_sel_E),
	
	.out_o(regdst_E)
);

syncE2M syncE2M0
(
	.exc_mask_i(exc_mask_E | alu_exc_E |fetch_exc_E),
	.is_delayslot_i(is_delayslot_E),
	.int_i(int_i),
	.cp0_wen_i(cp0_wen_E),
	.str_type_i(str_type_E),
	.load_type_i(load_type_E),
	.hisrc_sel_i(hisrc_sel_E),
	.losrc_sel_i(losrc_sel_E),
	.hiwe_i(hiwe_E & alu_we_E),
	.lowe_i(lowe_E & alu_we_E),
	.regwe_i(regwe_E & alu_we_E),
	.rlt_sel_i(rlt_sel_E),
	.memwe_i(alu_mwe_E & memwe_E),
	
	
	.sc_rlt_i(alu_mwe_E),
	.regdat0_i(regdat0_E),
	.regdat1_i(regdat1_E),
	.aluout_i(aluout_E),
	.byte_vld_i(byte_vld_E),
	.mulrlt_i(is_div_E ? divrlt_E :mulrlt_E),
		
	.regdst_i(regdst_E),
	
	.pc_plus4_i(pc_plus4_E),
	
//-----------------------------------
	.exc_mask_o(exc_mask_M),
	.is_delayslot_o(is_delayslot_M),
	.int_o(int_M),
	.cp0_wen_o(cp0_wen_M),
	.str_type_o(str_type_M),
	.load_type_o(load_type_M),
	.hisrc_sel_o(hisrc_sel_M),
	.losrc_sel_o(losrc_sel_M),
	.hiwe_o(hiwe_M),
	.lowe_o(lowe_M),
	.regwe_o(regwe_M),
	.rlt_sel_o(rlt_sel_M),
	.memwe_o(memwe_M),
	
	.sc_rlt_o(sc_rlt_M),
	.regdat0_o(regdat0_M),
	.regdat1_o(regdat1_M),
	.aluout_o(aluout_M),
	.byte_vld_o(byte_vld_M),
	.mulrlt_o(mulrlt_M),
		
	.regdst_o(regdst_M),
	
	.pc_plus4_o(pc_plus4_M),
	
	.irq_i(exc_en),
	.stall0_i(1'b0),
	.stall1_i(stcl_ram_cache),
	.clr_i(stcl_div),
	.clk(clk),
	.rst_n(rst_n)
	
);

mod_data_pre mod_data_pre0
(
	.in(regdat1_M),
	.str_type_i(str_type_M),
	
	.byte_vld_i(byte_vld_M),
	
	.out(mod_data_M)
);
/*
ram ram0
(
	.addr_i(aluout_M),
	.wd_i(mod_data_M),
	.wvld_i(byte_vld_M),
	.en(memwe_M),
	
	.dout_o(read_data_M),
	
	.clk(clk),
	.rst_n(rst_n)
);*/


mux_21 fw_hi_mux_M
(
	.in0_i(aluout_M),
	.in1_i(mulrlt_M[63:32]),
	
	.sel_i(hisrc_sel_M),
	
	.out_o(hi_M)
);

mux_21 fw_lo_mux_M
(
	.in0_i(aluout_M),
	.in1_i(mulrlt_M[31:0]),
	
	.sel_i(losrc_sel_M),
	
	.out_o(lo_M)
);

assign ll_bit_M = (load_type_M == `LOAD_LL);

syncM2W syncM2W0
(
	.cp0_wen_i(cp0_wen_M),
	.load_type_i(load_type_M),
	.hisrc_sel_i(hisrc_sel_M),
	.losrc_sel_i(losrc_sel_M),
	.hiwe_i(hiwe_M),
	.lowe_i(lowe_M),
	.reg_we_i(regwe_M),
	.rlt_sel_i(rlt_sel_M),
	
	.sc_rlt_i(sc_rlt_M),
	.read_data_i(read_data_M),
	.aluout_i(aluout_M),
	.mulrlt_i(mulrlt_M),
	.regdst_i(regdst_M),
	.regdat1_i(regdat1_M),
	.byte_vld_i(byte_vld_M),
	
	
	.cp0_wen_o(cp0_wen_W),
	.load_type_o(load_type_W),
	.hisrc_sel_o(hisrc_sel_W),
	.losrc_sel_o(losrc_sel_W),
	.hiwe_o(hiwe_W),
	.lowe_o(lowe_W),
	.regwe_o(regwe_W),
	.rlt_sel_o(rlt_sel_W),
	
	.sc_rlt_o(sc_rlt_W),
	.read_data_o(read_data_W),
	.aluout_o(aluout_W),
	.mulrlt_o(mulrlt_W),
	.regdst_o(regdst_W),
	.regdat1_o(regdat1_W),
	.byte_vld_o(byte_vld_W),
	
	.irq_i(exc_en),
	.stall0_i(stcl_ram_cache),
	.clk(clk),
	.rst_n(rst_n)
	
);

mod_data_post mod_data_post0
(
	.in(read_data_W),
	.out(sign_data_W),

	.load_type_i(load_type_W),
	
	.byte_vld_i(byte_vld_W),
	.regdat_i(regdat1_W)
	
);

mux_41 rlt_mux
(
	.in0_i(sign_data_W),
	.in1_i(aluout_W),
	.in2_i({31'b0, sc_rlt_W}),
	.in3_i(aluout_W),
	
	.sel_i(rlt_sel_W),
	
	.out_o(rlt_W)
);

mux_21 hi_mux
(
	.in0_i(rlt_W),
	.in1_i(mulrlt_W[63:32]),
	
	.sel_i(hisrc_sel_W),
	
	.out_o(hi_W)
);

mux_21 lo_mux
(
	.in0_i(rlt_W),
	.in1_i(mulrlt_W[31:0]),
	
	.sel_i(losrc_sel_W),
	
	.out_o(lo_W)
);

assign llbit_set_W = (load_type_W == `LOAD_LL);

ll_bit ll_bit0
(
	.ll_bit_i(llbit_set_W),
	.we_i(llbit_set_W),
	
	.clr_i(exc_en),
	
	.ll_bit_o(ll_bit_W),
	
	.clk(clk),
	.rst_n(rst_n)
);

conf_rslt conf_rslt0
(
	.rs_i_E(rs_E),
	.rt_i_E(rt_E),
	.regdst_i_M(regdst_M),
	.regwe_i_M(regwe_M),
	.regdst_i_W(regdst_W),
	.regwe_i_W(regwe_W),

	.fw_rd0_sel_o(fw_rd0_sel),
	.fw_rd1_sel_o(fw_rd1_sel),
//---------------------------------	
	
	.hiwe_i_M(hiwe_M),
	.lowe_i_M(lowe_M),
	.hiwe_i_W(hiwe_W),
	.lowe_i_W(lowe_W),
	
	.fw_hi_sel_o(fw_hi_sel),
	.fw_lo_sel_o(fw_lo_sel),
	
	
	.rd_i_E(rd_E),
	.cp0_wen_i_M(cp0_wen_M),
	.cp0_wen_i_W(cp0_wen_W),
	
	.fw_cp0_sel_o(fw_cp0_sel),

	
	.regdst_i_E(regdst_E),
	.rs_i_D(ir_D[25:21]),
	.rt_i_D(ir_D[20:16]),
	.load_type_i_E(load_type_E),
	.str_type_i_E(str_type_E),
	
	.stcl_lw_o(stcl_lw),
//---------------------------------	
	.is_bj_inst_reg_i_D(is_bj_inst_reg_D),
	.regwe_i_E(regwe_E),
	.load_type_i_M(load_type_M),
	.str_type_i_M(str_type_M),
	
	.fw_jrd0_sel_o(fw_jrd0_sel),
	.fw_jrd1_sel_o(fw_jrd1_sel),
	.stcl_jmp_o(stcl_jmp),
	
	.rst_n(rst_n)
);


get_vic_inst_info get_vic_inst_info
(
	.pc_i_F(instaddr),
	.pc_plus4_i_D(pc_plus4_D),
	.pc_plus4_i_E(pc_plus4_E),
	.pc_plus4_i_M(pc_plus4_M),
	
	.is_delayslot_i_D(is_delayslot_D),
	.is_delayslot_i_E(is_delayslot_E),
	.is_delayslot_i_M(is_delayslot_M),
	
	.vic_is_delayslot_o(vic_is_delayslot),
	.vic_addr_o(vic_addr)
);

/*********************************************************************************************
for sopc
*********************************************************************************************/
/*
assign instdata = (!stcl_icache) ? inst_cache : inst_rom;
assign stcl_f = stcl_icache & stcl_rom;
icache
(
	// for cpu
	.addr_i(instaddr),
	.en(1'b1),
	
	//for wb_rom
	.data_i(instdata),
	.clr_i(exc_en),
	.ack_i(stcl_rom),	
	
	.dout_o(inst_cache),
	.stall_o(stcl_icache),
	
	.clk(clk),
	.clk_cache(cache_clk),
	.rst_n(rst_n)
);
*/



/*
wire priram_en;
wire[31:0] priram_data;
wire[31:0] priram_addr;
wire priram_we;
wire[3:0] priram_sel;
wire[31:0] priram_rdata;
wire priram_stall;
wire dcache_interrupt;
wire use_dcache_data;

wire[31:0] rdata_dcache;
wire[31:0] rdata_ram;
wire stcl_ram;
wire stcl_dcache;

assign read_data_M = use_dcache_data ? rdata_dcache : rdata_ram;
assign stcl_ram_cache = stcl_dcache | stcl_ram;

dcache
(
	// for cpu
	.addr_i(aluout_M),
	.wd_i(mod_data_M),
	.wvld_i(byte_vld_M),
	.we(memwe_M),
	.en((load_type_M != 4'd0) || (str_type_M != 4'd0)),
	
	.dout_o(rdata_dcache),
	.interrupt_o(dcache_interrupt),
	.stall_o(stcl_dcache),
	.use_dcache_data_o(use_dcache_data),
	
	//for wb_ram
	.data_i(rdata_ram),
	.ram_stall_i(stcl_ram),	
	
	//for pri
	.pri_en_o(priram_en),
	.pri_data_o(priram_data),
	.pri_addr_o(priram_addr),
	.pri_we_o(priram_we),
	.pri_sel_o(priram_sel),
	.pri_wb_stall_i(priram_stall),
	
	.flag(flag),
	
	.clr0_i(exc_en),
	.clk(clk),
	.dcache_clk(cache_clk),
	.rst_n(rst_n)
);

wishbone_apt wishbone_priram
(
	.cpu_en_i(priram_en),
	.cpu_data_i(priram_data),
	.cpu_addr_i(priram_addr),
	.cpu_we_i(priram_we),
	.cpu_sel_i(priram_sel),
	.cpu_data_o(priram_rdata),
	
	.wb_data_i(priram_data_i),
	.wb_ack_i(priram_ack_i),
	.wb_addr_o(priram_addr_o),
	.wb_data_o(priram_data_o),
	.wb_we_o(priram_we_o),
	.wb_sel_o(priram_sel_o),
	.wb_stb_o(priram_stb_o),
	.wb_cyc_o(priram_cyc_o),

	.stall_o(priram_stall),

	.stall0_i(1'b0),	//sync_M2W
	.clr0_i(1'b0),		//same to sync_M2W
	
	.clk(clk),
	.rst_n(rst_n)
);

*/
//wire[31:0] rdata_ram;
assign read_data_M = rdata_ram;
assign stcl_ram_cache = 1'b0;


/*
flash flash0
(
	// to core
	.addr_i(),
	
	.rdy_o(),
	.data_o(),

	// to flash
	.data_i(),
	
	.oen_o(rom_oen_o),
	.rstn_o(rom_rstn_o),
	.wen_o(rom_wen_o),
	.addr_o(rom_addr_o),

	.clk(clk),
	.rst_n(rst_n)
);*/

endmodule
