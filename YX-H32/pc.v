module pc
(
	//常规跳转接口
	input[31:0] pc_load_i,
	input pc_sel_i,
	//中断接口（优先）
	input pc_sel_exc_i,
	input[31:0] pc_load_exc_i,
	
	output reg[31:0] pc_o,
	output[31:0] pc_plus4_o,
	output[7:0]	fetch_exc_o,
	
	input stall0_i,
	input stall1_i,
	input stall2_i,
	input stall3_i,
	input rst_n,
	input clk
);

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
		pc_o <= 32'hbfc00000;
	else if(pc_sel_exc_i)
		pc_o <= pc_load_exc_i;
	else if(stall0_i || stall1_i || stall2_i || stall3_i)
		;//do nothing
	else if(pc_sel_i)
		pc_o <= pc_load_i;
	else
		pc_o <= pc_plus4_o;	
end

assign pc_plus4_o = pc_o + 32'd4;
assign fetch_exc_o=(pc_o[1:0]==2'd0)?8'd0:8'b01000000;
endmodule
