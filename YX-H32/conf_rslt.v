`include "./defines.v"
module conf_rslt
(
	// 寄存器数据前推
	input[4:0] rs_i_E,
	input[4:0] rt_i_E,
	input[4:0] regdst_i_M,
	input regwe_i_M,
	input[4:0] regdst_i_W,
	input regwe_i_W,
	
	output reg[1:0] fw_rd0_sel_o,
	output reg[1:0] fw_rd1_sel_o,
	
	// hilo数据前推
	input hiwe_i_M,
	input lowe_i_M,
	input hiwe_i_W,
	input lowe_i_W,
	
	output reg[1:0] fw_hi_sel_o,
	output reg[1:0] fw_lo_sel_o,
	
	// cp0数据前推
	input[4:0] rd_i_E,
	input cp0_wen_i_M,
	input cp0_wen_i_W,
	
	output reg[1:0] fw_cp0_sel_o,
	
	// LOAD阻塞
	input[4:0] regdst_i_E,
	input[4:0] rs_i_D,
	input[4:0] rt_i_D,
	input[3:0] load_type_i_E,
	input[3:0] str_type_i_E,
	
	output reg stcl_lw_o,
	
	// 跳转阻塞
	input is_bj_inst_reg_i_D,
	input regwe_i_E,
	input[3:0] load_type_i_M,
	input[3:0] str_type_i_M,
	
	output reg[1:0] fw_jrd0_sel_o,
	output reg[1:0] fw_jrd1_sel_o,
	output reg stcl_jmp_o,
	
	
	
	input rst_n
);

always @(*)
begin
	if(!rst_n)
	begin
		fw_rd0_sel_o = 2'b00;
	end
	else if((rs_i_E != 5'd0) && (rs_i_E == regdst_i_M) && (regwe_i_M))
		fw_rd0_sel_o = 2'b01;
	else if((rs_i_E != 5'd0) && (rs_i_E == regdst_i_W) && (regwe_i_W))
		fw_rd0_sel_o = 2'b10;
	else
		fw_rd0_sel_o = 2'b00;
		
end

always @(*)
begin
	if(!rst_n)
	begin
		fw_rd1_sel_o = 2'b00;
	end
	else if((rt_i_E != 5'd0) && (rt_i_E == regdst_i_M) && (regwe_i_M))
		fw_rd1_sel_o = 2'b01;
	else if((rt_i_E != 5'd0) && (rt_i_E == regdst_i_W) && (regwe_i_W))
		fw_rd1_sel_o = 2'b10;
	else
		fw_rd1_sel_o = 2'b00;
		
end

always @(*)
begin
	if(!rst_n)
	begin
		fw_hi_sel_o = 2'b00;
	end
	else if(hiwe_i_M)
		fw_hi_sel_o = 2'b01;
	else if(hiwe_i_W)
		fw_hi_sel_o = 2'b10;
	else
		fw_hi_sel_o = 2'b00;
		
end

always @(*)
begin
	if(!rst_n)
	begin
		fw_lo_sel_o = 2'b00;
	end
	else if(lowe_i_M)
		fw_lo_sel_o = 2'b01;
	else if(lowe_i_W)
		fw_lo_sel_o = 2'b10;
	else
		fw_lo_sel_o = 2'b00;
		
end


always @(*)
begin
	if(!rst_n)
		fw_cp0_sel_o = 2'd0;
	else if((rd_i_E == regdst_i_M) &&cp0_wen_i_M)
		fw_cp0_sel_o = 2'd1;
	else if((rd_i_E == regdst_i_W) &&cp0_wen_i_W)
		fw_cp0_sel_o = 2'd2;
	else
		fw_cp0_sel_o = 2'd0;
end





always @(*)
begin
	if(!rst_n)
		stcl_lw_o = 1'b0;
	else if(((rs_i_D == regdst_i_E) || (rt_i_D == regdst_i_E)) && (load_type_i_E != 4'd0))
		stcl_lw_o = 1'b1;
	else if(((rs_i_D == regdst_i_E) || (rt_i_D == regdst_i_E)) && (str_type_i_E == `STR_SC))
		stcl_lw_o = 1'b1;
	else
		stcl_lw_o = 1'b0;
		
end


always @(*)
begin
	if(!rst_n)
		stcl_jmp_o = 1'b0;
	else if(is_bj_inst_reg_i_D)
		if(((rs_i_D == regdst_i_E) || (rt_i_D == regdst_i_E)) && regwe_i_E)	//执行阶段相关的ALU指令、LOAD指令、sc指令
			stcl_jmp_o = 1'b1;
		else if(((rs_i_D == regdst_i_M) || (rt_i_D == regdst_i_M)) && (load_type_i_M != 4'd0))	//访存阶段相关的LOAD指令
			stcl_jmp_o = 1'b1;
		else if(((rs_i_D == regdst_i_M) || (rt_i_D == regdst_i_M)) && (str_type_i_M == `STR_SC))	//访存阶段相关的sc指令
			stcl_jmp_o = 1'b1;
		else
			stcl_jmp_o = 1'b0;
	else
		stcl_jmp_o = 1'b0;
end

always @(*)
begin
	if(!rst_n)
	begin
		fw_jrd0_sel_o = 2'b00;
	end
	else if((rs_i_D != 5'd0) && (rs_i_D == regdst_i_M) && (regwe_i_M))
		fw_jrd0_sel_o = 2'b01;
	else if((rs_i_D != 5'd0) && (rs_i_D == regdst_i_W) && (regwe_i_W))
		fw_jrd0_sel_o = 2'b10;
	else
		fw_jrd0_sel_o = 2'b00;
		
end

always @(*)
begin
	if(!rst_n)
	begin
		fw_jrd1_sel_o = 2'b00;
	end
	else if((rt_i_D != 5'd0) && (rt_i_D == regdst_i_M) && (regwe_i_M))
		fw_jrd1_sel_o = 2'b01;
	else if((rt_i_D != 5'd0) && (rt_i_D == regdst_i_W) && (regwe_i_W))
		fw_jrd1_sel_o = 2'b10;
	else
		fw_jrd1_sel_o = 2'b00;
		
end



endmodule
