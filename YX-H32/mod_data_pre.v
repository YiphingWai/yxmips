`include "./defines.v"
module mod_data_pre
(
	input[31:0] in,
	input[3:0] str_type_i,
	
	input[3:0] byte_vld_i,
	
	output reg[31:0] out
);

always @(*)
begin
	out = in;
	case (str_type_i)
	
	`STR_SB:
		case(byte_vld_i)
		4'b0001:
			out = {8'b0, 8'b0, 8'b0, in[7:0]};
		4'b0010:
			out = {8'b0, 8'b0, in[7:0], 8'b0};
		4'b0100:
			out = {8'b0, in[7:0], 8'b0, 8'b0};
		default:
			out = {in[7:0], 8'b0, 8'b0, 8'b0};
		endcase
	`STR_SH:
		out = (byte_vld_i == 4'b0011) ? {8'b0, 8'b0, in[15:8], in[7:0]} : {in[15:8], in[7:0], 8'b0, 8'b0};
	`STR_SC,
	`STR_SW:
		;// ʲôҲ����
	`STR_SWL:
		case(byte_vld_i)
		
		4'b0001:
			out = {24'b0, in[31:24]};
		4'b0011:
			out = {16'b0, in[31:16]};
		4'b0111:
			out = {8'b0, in[31:8]};
		default:
			out = in;
		
		endcase
	`STR_SWR:
		case(byte_vld_i)
		
		4'b1000:
			out = {in[7:0], 24'b0};
		4'b1100:
			out = {in[15:0], 16'b0};
		4'b1110:
			out = {in[23:0], 8'b0};
		default:
			out = in;
		
		endcase
	endcase
	
end

endmodule
