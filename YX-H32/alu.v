`include "./defines.v"

module alu
(
	input[7:0] alucode_i,
	input[31:0] src0_i,
	input[31:0] src1_i,
	input ll_bit_i,
	input[63:0] hilo_i,
	input[31:0] pc_plus8_i,
	input[31:0] cp0_i,
	
	output reg[31:0] aluout_o,
	output alu_we_o,
	output reg alu_mwe_o,
	output reg[63:0] mulrlt_o,
	output reg[3:0] byte_vld_o,	//±êÊ¾4×Ö½ÚÖÐÄÄÐ©ÊÇÒªÐ´ÈëµÄ
	output reg[7:0] alu_exc_o
	
	
);

wire[31:0] src0 = src0_i;
wire[31:0] src1 = src1_i;

reg reg_we_mov;
wire reg_we_arithm;

reg[31:0] logic_rlt;
reg[31:0] shift_rlt;
reg[31:0] mov_rlt;
reg[31:0] load_rlt;
reg[31:0] arithm_rlt;
reg[31:0] cp0_rlt;
wire[63:0] mulrlt;

//Âß¼­ÔËËã
always @(*)
begin
	case (alucode_i)
	
	// Âß¼­ÔËËã
	`ALUOP_AND,
	`ALUOP_ANDI:
		logic_rlt = src0 & src1;
	`ALUOP_OR,
	`ALUOP_ORI:
		logic_rlt = src0 | src1;
	`ALUOP_XOR,
	`ALUOP_XORI:
		logic_rlt = src0 ^ src1;
	`ALUOP_NOR:
		logic_rlt = ~(src0 | src1);
	`ALUOP_LU:	
		logic_rlt = {src1[15:0], 16'd0};
	default:
		logic_rlt = 32'b0;
		
	endcase
end

// ÒÆÎ»ÔËËã
always @(*)
begin
	case (alucode_i)
	
	// ÒÆÎ»ÔËËã
	`ALUOP_SLL:
		shift_rlt = src1 << src0[10:6];
	`ALUOP_SRL:
		shift_rlt = src1 >> src0[10:6];
	`ALUOP_SRA:
		shift_rlt = ({32{src1[31]}} << (6'd32-{1'b0, src0[10:6]})) | (src1 >> src0[10:6]);
	`ALUOP_SLLV:
		shift_rlt = src1 << src0[4:0];
	`ALUOP_SRLV:
		shift_rlt = src1 >> src0[4:0];
	`ALUOP_SRAV:
		shift_rlt = ({32{src1[31]}} << (6'd32-{1'b0, src0[4:0]})) | (src1 >> src0[4:0]);
	default:
		shift_rlt = 32'b0;
		
	endcase
end

// MOV
always @(*)
begin
	reg_we_mov = 1'b1;
	case (alucode_i)
		
	// movÔËËã
	`ALUOP_MOVN:
	begin
		mov_rlt = src0;
		reg_we_mov = (src1==0)?1'b0:1'b1;
	end
	`ALUOP_MOVZ:
	begin
		mov_rlt = src0;
		reg_we_mov = (src1==0)?1'b1:1'b0;
	end
	`ALUOP_MFHI:
	begin
		mov_rlt = src1;
		reg_we_mov = 1'b1;
	end
	`ALUOP_MFLO:
	begin
		mov_rlt = src1;
		reg_we_mov = 1'b1;
	end
	`ALUOP_MTHI:
	begin
		mov_rlt = src0;
		reg_we_mov = 1'b1;
	end
	`ALUOP_MTLO:
	begin
		mov_rlt = src0;
		reg_we_mov = 1'b1;
	end	
	default:
		mov_rlt = 32'b0;
	
	endcase
	
end

// ËãÊýÔËËã
wire[31:0] sec_opnum = ((alucode_i == `ALUOP_SUB) || (alucode_i == `ALUOP_SUBU) ||
								(alucode_i == `ALUOP_SLT) || (alucode_i == `ALUOP_SLTI)||(alucode_i == `ALUOP_TLT) ||
	                      (alucode_i == `ALUOP_TGE)) ? (~src1)+1 : src1;
								
wire[31:0] result_sum = src0 + sec_opnum;

wire add_ov = (((!src0[31] && !sec_opnum[31]) && result_sum[31]) ||((src0[31] && sec_opnum[31]) && (!result_sum[31])));
wire sub_ov = add_ov | (!src0[31] && src1==32'h80000000);

wire alu_ov = ((alucode_i == `ALUOP_ADD) || (alucode_i == `ALUOP_ADDI)) ? add_ov
					: (alucode_i == `ALUOP_SUB) ? sub_ov : 1'b0;  

wire src0_lt_src1_sign = ((src0[31] && !src1[31]) ||
									(!src0[31] && !src1[31] && result_sum[31])|| 
									(src0[31] && src1[31] && result_sum[31]));

wire[31:0] src0_n = ~src0;

assign alu_we_o = reg_we_mov & (~alu_ov);

// ÓÐ·ûºÅ³Ë·¨£¬Ò»ÂÉÏÈ»¯ÎªÕýÊý×öÎÞ·ûºÅ³ËºóÈô½á¹ûÓ¦Îª¸ºÔÙ¶Ô½á¹ûÇó²¹Âë
wire is_sign_mul = (alucode_i == `ALUOP_MUL) || (alucode_i == `ALUOP_MULT) || (alucode_i == `ALUOP_MADD) || (alucode_i == `ALUOP_MSUB);
wire[31:0] mul_op0 = (is_sign_mul && (src0[31]==1'b1)) ? ((~src0) + 1) : src0;
wire[31:0] mul_op1 = (is_sign_mul && (src1[31]==1'b1)) ? ((~src1) + 1) : src1;
wire[63:0] mul_tmp = mul_op0 * mul_op1;
assign mulrlt = (is_sign_mul && (src0[31]^src1[31])) ? (~mul_tmp)+64'd1 : mul_tmp;
//for ³ËÀÛ¼Ó/¼õÖ¸Áî
wire[63:0] mulrlt_n = (~mulrlt) + 64'd1;
wire[63:0] mulacc = ((alucode_i == `ALUOP_MSUB) || (alucode_i == `ALUOP_MSUBU)) ? (hilo_i + mulrlt_n) : (hilo_i + mulrlt);
always @(*)
begin
	arithm_rlt = 32'b0;
	mulrlt_o = 64'b0;
	case (alucode_i)
	
	// ËãÊýÔËËã
	`ALUOP_ADD,
	`ALUOP_ADDU,
	`ALUOP_SUB,
	`ALUOP_SUBU,
	`ALUOP_ADDI,
	`ALUOP_ADDIU:
		arithm_rlt = result_sum;
	
	`ALUOP_SLT,
	`ALUOP_SLTI:
		arithm_rlt = {31'b0, src0_lt_src1_sign};
	
	`ALUOP_SLTU,
	`ALUOP_SLTIU:
		arithm_rlt = {31'b0, (src0 < src1)};
		
	`ALUOP_CLZ:
		arithm_rlt = src0[31] ? 0 : src0[30] ? 1 : src0[29] ? 2 :
						src0[28] ? 3 : src0[27] ? 4 : src0[26] ? 5 :
						src0[25] ? 6 : src0[24] ? 7 : src0[23] ? 8 : 
						src0[22] ? 9 : src0[21] ? 10 : src0[20] ? 11 :
						src0[19] ? 12 : src0[18] ? 13 : src0[17] ? 14 : 
						src0[16] ? 15 : src0[15] ? 16 : src0[14] ? 17 : 
						src0[13] ? 18 : src0[12] ? 19 : src0[11] ? 20 :
						src0[10] ? 21 : src0[9] ? 22 : src0[8] ? 23 : 
						src0[7] ? 24 : src0[6] ? 25 : src0[5] ? 26 : 
						src0[4] ? 27 : src0[3] ? 28 : src0[2] ? 29 : 
						src0[1] ? 30 : src0[0] ? 31 : 32 ;
	`ALUOP_CLO:
		arithm_rlt = src0_n[31] ? 0 : src0_n[30] ? 1 : src0_n[29] ? 2 :
						src0_n[28] ? 3 : src0_n[27] ? 4 : src0_n[26] ? 5 :
						src0_n[25] ? 6 : src0_n[24] ? 7 : src0_n[23] ? 8 : 
						src0_n[22] ? 9 : src0_n[21] ? 10 : src0_n[20] ? 11 :
						src0_n[19] ? 12 : src0_n[18] ? 13 : src0_n[17] ? 14 : 
						src0_n[16] ? 15 : src0_n[15] ? 16 : src0_n[14] ? 17 : 
						src0_n[13] ? 18 : src0_n[12] ? 19 : src0_n[11] ? 20 :
						src0_n[10] ? 21 : src0_n[9] ? 22 : src0_n[8] ? 23 : 
						src0_n[7] ? 24 : src0_n[6] ? 25 : src0_n[5] ? 26 : 
						src0_n[4] ? 27 : src0_n[3] ? 28 : src0_n[2] ? 29 : 
						src0_n[1] ? 30 : src0_n[0] ? 31 : 32;
	`ALUOP_MUL:
		arithm_rlt = mulrlt[31:0];
	
	`ALUOP_MULT,
	`ALUOP_MULTU:
		mulrlt_o = mulrlt;
	
	`ALUOP_MADD,
	`ALUOP_MADDU,
	`ALUOP_MSUB,
	`ALUOP_MSUBU:
		mulrlt_o = mulacc;

	endcase
end





// ¼ÓÔØÖ¸Áî
// ÒÔÏÂÁ½ÌõÎªlwl¡¢lwr¡¢swl¡¢swrÉè¼Æ
wire[3:0] byte_vldp = (result_sum[1:0]==2'd0) ? 4'b0001 : (result_sum[1:0]==2'd1) ? 4'b0011 : (result_sum[1:0]==2'd2) ? 4'b0111 : 4'b1111;	
wire[3:0] byte_vldn = (result_sum[1:0]==2'd0) ? 4'b1111 : (result_sum[1:0]==2'd1) ? 4'b1110 : (result_sum[1:0]==2'd2) ? 4'b1100 : 4'b1000;
// ÒÔÏÂÎª×Ö½ÚÖ¸ÁîÉè¼Æ
wire[3:0] byte_vldb = (result_sum[1:0]==2'd0) ? 4'b0001 : (result_sum[1:0]==2'd1) ? 4'b0010 : (result_sum[1:0]==2'd2) ? 4'b0100 : 4'b1000;
// ÒÔÏÂÎª°ë×ÖÖ¸ÁîÉè¼Æ
wire[3:0] byte_vldh = result_sum[1] ? 4'b1100 : 4'b0011;

always @(*)
begin
	load_rlt = 32'd0;
	byte_vld_o = 4'b0;
	alu_mwe_o = 1'b1;
	case (alucode_i)
	`ALUOP_LB,
	`ALUOP_LBU:
	begin
		byte_vld_o = byte_vldb;
		load_rlt = result_sum;
	end
	`ALUOP_LH,
	`ALUOP_LHU:
	begin
		byte_vld_o = byte_vldh;
		load_rlt = result_sum;
	end
	`ALUOP_LW:
	begin
		byte_vld_o = 4'b1111;
		load_rlt = result_sum;
	end
	`ALUOP_LWL:
	begin
		byte_vld_o = byte_vldp;
		load_rlt = result_sum;
	end
	`ALUOP_LWR:
	begin
		byte_vld_o = byte_vldn;
		load_rlt = result_sum;
	end
	
	`ALUOP_SB:
	begin
		byte_vld_o = byte_vldb;
		load_rlt = result_sum;
	end
	`ALUOP_SH:
	begin
		byte_vld_o = byte_vldh;
		load_rlt = result_sum;
	end
	`ALUOP_SW:
	begin
		byte_vld_o = 4'b1111;
		load_rlt = result_sum;
	end
	`ALUOP_SWL:
	begin
		byte_vld_o = byte_vldp;
		load_rlt = result_sum;
	end
	`ALUOP_SWR:
	begin
		byte_vld_o = byte_vldn;
		load_rlt = result_sum;
	end
	
	`ALUOP_LL:
		load_rlt = result_sum;
	`ALUOP_SC:
	begin
		byte_vld_o = 4'b1111;
		load_rlt = result_sum;
		if(!ll_bit_i)
			alu_mwe_o = 1'b0;
	end
	
	endcase
end

//cp0´æÈ¡Ö¸Áî
always @(*)
begin
	cp0_rlt = 32'd0;
	case(alucode_i)
	`ALUOP_MTC:
		cp0_rlt = src1;
	`ALUOP_MFC:
		cp0_rlt = cp0_i;
	endcase
end



							
always @(*)
begin
	case (alucode_i)
	//Âß¼­ÔËËã
	`ALUOP_AND,
	`ALUOP_ANDI,
	`ALUOP_OR,
	`ALUOP_ORI,
	`ALUOP_XOR,
	`ALUOP_XORI,
	`ALUOP_NOR,
	`ALUOP_LU:	
		aluout_o = logic_rlt;
		
	// ÒÆÎ»ÔËËã
	`ALUOP_SLL,
	`ALUOP_SRL,
	`ALUOP_SRA,
	`ALUOP_SLLV,
	`ALUOP_SRLV,
	`ALUOP_SRAV:
		aluout_o = shift_rlt;
		
	// movÔËËã
	`ALUOP_MOVN,
	`ALUOP_MOVZ,
	`ALUOP_MFHI,
	`ALUOP_MFLO,
	`ALUOP_MTHI,
	`ALUOP_MTLO:
		aluout_o = mov_rlt;
	
	// ËãÊýÔËËã
	`ALUOP_ADD,
	`ALUOP_ADDU,
	`ALUOP_SUB,
	`ALUOP_SUBU,
	`ALUOP_ADDI,
	`ALUOP_ADDIU,
	`ALUOP_SLT,
	`ALUOP_SLTI,
	`ALUOP_SLTU,
	`ALUOP_SLTIU,
	`ALUOP_CLZ,
	`ALUOP_CLO,
	`ALUOP_MUL,
	`ALUOP_MULT,
	`ALUOP_MULTU,
	`ALUOP_MADD,
	`ALUOP_MADDU,
	`ALUOP_MSUB,
	`ALUOP_MSUBU:
		aluout_o = arithm_rlt;
		
	// ¼ÓÔØ´æ´¢Ö¸Áî
	`ALUOP_LB,
	`ALUOP_LBU,
	`ALUOP_LH,
	`ALUOP_LHU,
	`ALUOP_LW,
	`ALUOP_LWL,
	`ALUOP_LWR,
	`ALUOP_SB,
	`ALUOP_SH,
	`ALUOP_SW,
	`ALUOP_SWL,
	`ALUOP_SWR,
	`ALUOP_LL,
	`ALUOP_SC:
		aluout_o = load_rlt;
		
	//Ìø×ªÖ¸Áî
	`ALUOP_JALR:
		aluout_o = pc_plus8_i;
		
	//cp0´æÈ¡Ö¸Áî
	`ALUOP_MTC,
	`ALUOP_MFC:
		aluout_o = cp0_rlt;
		
	default:
		aluout_o = 32'b0;
	
	endcase

end


//×ÔÏÝÖ¸Áî¡¢Òç³öÖÐ¶Ï
always @(*)
begin
	alu_exc_o = {4'b0, alu_ov, 3'b0};
	case(alucode_i)
	`ALUOP_TEQ:
		alu_exc_o[4] = (src0 == src1);
	`ALUOP_TGE:
		alu_exc_o[4] = ~src0_lt_src1_sign;
	`ALUOP_TGEU:
		alu_exc_o[4] = (src0 >= src1);
	`ALUOP_TLT:
		alu_exc_o[4] = src0_lt_src1_sign;
	`ALUOP_TLTU:
		alu_exc_o[4] = (src0 < src1);
	`ALUOP_TNE:
		alu_exc_o[4] = ~(src0 == src1);
	endcase
end





							
								
endmodule
