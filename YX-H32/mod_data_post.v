`include "./defines.v"
module mod_data_post
(
	input[31:0] in,
	input[31:0] regdat_i,

	input[3:0] load_type_i,
	
	input[3:0] byte_vld_i,
	
	output reg[31:0] out
	
);
// 为字节指令设计
wire[7:0] byte_data = (byte_vld_i == 4'b0001) ? in[7:0] : (byte_vld_i == 4'b0010) ? in[15:8] : (byte_vld_i == 4'b0100) ? in[23:16] : in[31:24];
// 为半字指令设计
wire[15:0] hb_data = (byte_vld_i == 4'b0011) ? in[15:0] : in[31:16];
always @(*)
begin
	out = in;
	case (load_type_i)
	
	`LOAD_LB:
		out = {{24{byte_data[7]}}, byte_data};
	`LOAD_LBU:
		out = {24'b0, byte_data};
	`LOAD_LH:
		out = {{16{hb_data[15]}}, hb_data[15:0]};
	`LOAD_LHU:
		out = {16'b0, hb_data[15:0]};
	`LOAD_LW,
	`LOAD_LL:
		out = in;
	`LOAD_LWL:
		case(byte_vld_i)
		4'b0001:
			out = {in[7:0], regdat_i[23:0]};
		4'b0011:
			out = {in[15:0], regdat_i[15:0]};
		4'b0111:
			out = {in[23:0], regdat_i[7:0]};
		default:
			out = in;
		endcase
	`LOAD_LWR:
		case(byte_vld_i)
		4'b1111:
			out = in;
		4'b1110:
			out = {regdat_i[31:24], in[31:8]};
		4'b1100:
			out = {regdat_i[31:16], in[31:16]};
		default:
			out = {regdat_i[31:8], in[31:24]};
		endcase
			
	endcase
end


endmodule
