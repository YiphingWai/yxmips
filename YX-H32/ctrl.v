`include "./defines.v"

module ctrl
(
	input[31:0] inst_i,
	// for 跳转指令
	input[31:0] rd0_i,
	input[31:0] rd1_i,
	input[31:0] pc_plus4_i,
	//for 延迟槽
	input is_bj_inst_i,
	
	output reg is_div_o,
	output reg is_sign_div_o,
	output reg[7:0] exc_mask_o,
	output is_delayslot_o,
	output reg cp0_wen_o,
	output reg[3:0] str_type_o,
	output reg[3:0] load_type_o,
	output reg hisrc_sel_o,
	output reg losrc_sel_o,
	output reg hiwe_o,
	output reg lowe_o,
	output reg regwe_o,
	output reg[1:0] rlt_sel_o,
	output reg memwe_o,
	output reg sign_o,
	output reg[7:0] alucode_o,
	output reg alusrc0_sel_o,
	output reg[1:0] alusrc1_sel_o,
	output reg[1:0] regdst_sel_o,
	// for 跳转指令
	output reg is_bj_inst_reg_o,
	output reg pc_sel_o,
	//output reg branch_en_o,
	output reg[31:0]pc_branch_o,
	
	//for 延迟槽
	output reg is_bj_inst_o,
	
	
	input rst_n
);

assign is_delayslot_o = is_bj_inst_i;

wire[5:0] icode_i = inst_i[31:26];
wire[5:0] opcode_i = inst_i[5:0];
wire[4:0] bzcode = inst_i[20:16];
wire[4:0] cpcode = inst_i[25:21];

//分配ALU控制指令
always @(*)
begin
	if(!rst_n)
	begin
		exc_mask_o = 8'b0;
		alucode_o = 8'b0;
	end
	else
	begin
		alucode_o = `ALUOP_NOP;
		exc_mask_o = 8'b0;
		case(icode_i)
		
		`ICODE_ANDI:
			alucode_o = `ALUOP_ANDI;
		`ICODE_XORI:
			alucode_o = `ALUOP_XORI;
		`ICODE_LUI:
			begin
				alucode_o = `ALUOP_LU;
				exc_mask_o[2]= inst_i[25:21]!=5'd0;
			end
		`ICODE_ORI:
			alucode_o = `ALUOP_ORI;
		`ICODE_PREF:
			alucode_o = `ALUOP_NOP;
		`ICODE_ADDI:
			alucode_o = `ALUOP_ADDI;
		`ICODE_ADDIU:
			alucode_o = `ALUOP_ADDIU;
		`ICODE_SLTI:
			alucode_o = `ALUOP_SLTI;
		`ICODE_SLTIU:
			alucode_o = `ALUOP_SLTIU;
		`ICODE_LB:
			alucode_o = `ALUOP_LB;
		`ICODE_LBU:
			alucode_o = `ALUOP_LBU;
		`ICODE_LH:
			alucode_o = `ALUOP_LH;
		`ICODE_LHU:
			alucode_o = `ALUOP_LHU;
		`ICODE_LW:
			alucode_o = `ALUOP_LW;
		`ICODE_LWL:
			alucode_o = `ALUOP_LWL;
		`ICODE_LWR:
			alucode_o = `ALUOP_LWR;	
		`ICODE_SB:
			alucode_o = `ALUOP_SB;	
		`ICODE_SH:
			alucode_o = `ALUOP_SH;
		`ICODE_SW:
			alucode_o = `ALUOP_SW;	
		`ICODE_SWL:
			alucode_o = `ALUOP_SWL;	
		`ICODE_SWR:
			alucode_o = `ALUOP_SWR;	
		`ICODE_LL:
			alucode_o = `ALUOP_LL;		
		`ICODE_SC:
			alucode_o = `ALUOP_SC;		
		`ICODE_J:
			alucode_o = `ALUOP_NOP;
		`ICODE_JAL:
			alucode_o = `ALUOP_JALR;
		`ICODE_BEQ:
			alucode_o = `ALUOP_NOP;
		`ICODE_BGTZ,
		`ICODE_BLEZ:
			begin
					exc_mask_o[2]= bzcode!=5'd0;
					alucode_o = `ALUOP_NOP;
			end
		`ICODE_BNE:
			alucode_o = `ALUOP_NOP;
			
			
			
			
		`ICODE_COP0:
			case(cpcode)
			`CPCODE_MTC:
				alucode_o = `ALUOP_MTC;
			`CPCODE_MFC:
				alucode_o = `ALUOP_MFC;
			
			default:
				if(inst_i == `INST_ERET)
					exc_mask_o[5] = 1'b1;
				else
					exc_mask_o[2] = 1'b1;
			endcase
		
		`ICODE_REGIMM:
			case(bzcode)
			
			`BZCODE_BLTZ,
			`BZCODE_BGEZ:
				alucode_o = `ALUOP_NOP;
			`BZCODE_BLTZAL,
			`BZCODE_BGEZAL:
				alucode_o = `ALUOP_JALR;
				
			`BZCODE_TEQI:
				alucode_o = `ALUOP_TEQ;
			`BZCODE_TGEI:
				alucode_o = `ALUOP_TGE;
			`BZCODE_TGEIU:
				alucode_o = `ALUOP_TGEU;
			`BZCODE_TLTI:
				alucode_o = `ALUOP_TLT;
			`BZCODE_TLTIU:
				alucode_o = `ALUOP_TLTU;
			`BZCODE_TNEI:
				alucode_o = `ALUOP_TNE;
				
			default:
				exc_mask_o[2] = 1'b1;
			endcase
			
		
		`ICODE_SPECIAL2:
			case(opcode_i)
			
			`OPCODE_CLZ:
				alucode_o = `ALUOP_CLZ;
			`OPCODE_CLO:
				alucode_o = `ALUOP_CLO;
			`OPCODE_MUL:
				alucode_o = `ALUOP_MUL;
			`OPCODE_MADD:
				alucode_o = `ALUOP_MADD;
			`OPCODE_MADDU:
				alucode_o = `ALUOP_MADDU;
			`OPCODE_MSUB:
				alucode_o = `ALUOP_MSUB;
			`OPCODE_MSUBU:
				alucode_o = `ALUOP_MSUBU;
			
			default:
				exc_mask_o[2] = 1'b1;
			
			endcase
		
		`ICODE_SPECIAL:
			case(opcode_i)
			
			`OPCODE_AND:
				alucode_o = `ALUOP_AND;
			`OPCODE_OR:
				alucode_o = `ALUOP_OR;
			`OPCODE_XOR:
				alucode_o = `ALUOP_XOR;
			`OPCODE_NOR:
				alucode_o = `ALUOP_NOR;
			`OPCODE_SYNC:
				alucode_o = `ALUOP_NOP;
			`OPCODE_SLL:
				alucode_o = `ALUOP_SLL;
			`OPCODE_SRL:
				alucode_o = `ALUOP_SRL;
			`OPCODE_SRA:
				alucode_o = `ALUOP_SRA;
			`OPCODE_SLLV:
				alucode_o = `ALUOP_SLLV;
			`OPCODE_SRLV:
				alucode_o = `ALUOP_SRLV;
			`OPCODE_SRAV:
				alucode_o = `ALUOP_SRAV;

			`OPCODE_MOVN:
				alucode_o = `ALUOP_MOVN;
			`OPCODE_MOVZ:
				alucode_o = `ALUOP_MOVZ;
			`OPCODE_MFHI:
				alucode_o = `ALUOP_MFHI;	
			`OPCODE_MFLO:
				alucode_o = `ALUOP_MFLO;
			`OPCODE_MTHI:
				alucode_o = `ALUOP_MTHI;
			`OPCODE_MTLO:
				alucode_o = `ALUOP_MTLO;
				
			`OPCODE_ADD:
				alucode_o = `ALUOP_ADD;
			`OPCODE_ADDU:
				alucode_o = `ALUOP_ADDU;
			`OPCODE_SUB:
				alucode_o = `ALUOP_SUB;
			`OPCODE_SUBU:
				alucode_o = `ALUOP_SUBU;
			`OPCODE_SLT:
				alucode_o = `ALUOP_SLT;
			`OPCODE_SLTU:
				alucode_o = `ALUOP_SLTU;
				
			`OPCODE_MULT:
				alucode_o = `ALUOP_MULT;
			`OPCODE_MULTU:
				alucode_o = `ALUOP_MULTU;
			
			`OPCODE_DIV,
			`OPCODE_DIVU:
				alucode_o = `ALUOP_NOP;
			
			`OPCODE_JR:
				alucode_o = `ALUOP_NOP;
			`OPCODE_JALR:
				alucode_o = `ALUOP_JALR;
				
			`OPCODE_TEQ:
				alucode_o = `ALUOP_TEQ;
			`OPCODE_TGE:
				alucode_o = `ALUOP_TGE;
			`OPCODE_TGEU:
				alucode_o = `ALUOP_TGEU;
			`OPCODE_TLT:
				alucode_o = `ALUOP_TLT;
			`OPCODE_TLTU:
				alucode_o = `ALUOP_TLTU;
			`OPCODE_TNE:
				alucode_o = `ALUOP_TNE;
				
			`OPCODE_SYSCALL:
				exc_mask_o[1] = 1'b1;

			`OPCODE_BREAK:
				exc_mask_o[0] = 1'b1;
				
				
			default:
				exc_mask_o[2] = 1'b1;
			
			endcase
		
		default:
			exc_mask_o[2] = 1'b1;
		endcase
	end
end

//分配其他控制指令
wire[31:0] dstaddr_j = {pc_plus4_i[31:28], inst_i[25:0], 2'b00};
wire[31:0] dstaddr_b = pc_plus4_i + {{14{inst_i[15]}}, inst_i[15:0], 2'b00 };
always @(*)
begin
	if(!rst_n)
	begin
		is_div_o = 1'b0;
		is_sign_div_o = 1'b0;
		cp0_wen_o = 1'b0;
		str_type_o = 4'b0;
		load_type_o = 4'b0;
		hisrc_sel_o = 1'b0;
		losrc_sel_o = 1'b0;
		hiwe_o = 1'b0;
		lowe_o = 1'b0;
		regwe_o = 1'b0;
		rlt_sel_o = 2'b0;
		memwe_o = 1'b0;
		sign_o = 1'b1;
		alusrc0_sel_o = 1'b0;
		alusrc1_sel_o = 2'b0;
		regdst_sel_o = 2'b0;
		is_bj_inst_reg_o = 1'b0;
		pc_sel_o = 1'b0;
		//branch_en_o = 1'b0;
		pc_branch_o = 32'b0;
		is_bj_inst_o = 1'b0;
	end
	else
	begin
		is_div_o = 1'b0;
		is_sign_div_o = 1'b0;
		cp0_wen_o = 1'b0;
		str_type_o = 4'b0;
		load_type_o = 4'b0;
		hisrc_sel_o = 1'b0;
		losrc_sel_o = 1'b0;
		hiwe_o = 1'b0;
		lowe_o = 1'b0;
		regwe_o = 1'b0;
		rlt_sel_o = 2'b0;
		memwe_o = 1'b0;
		sign_o = 1'b1;
		alusrc0_sel_o = 1'b0;
		alusrc1_sel_o = 2'b0;
		regdst_sel_o = 2'b0;
		is_bj_inst_reg_o = 1'b0;
		pc_sel_o = 1'b0;
		//branch_en_o = 1'b0;
		pc_branch_o = 32'b0;
		is_bj_inst_o = 1'b0;
		case(icode_i)
		
		`ICODE_ANDI,
		`ICODE_XORI,
		`ICODE_LUI,
		`ICODE_ORI:
		begin
			regwe_o = 1'b1;
			rlt_sel_o = 2'b01;
			sign_o = 1'b0;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_PREF:
		begin
			//什么也不做
		end
		
		`ICODE_ADDI,
		`ICODE_ADDIU,
		`ICODE_SLTI,
		`ICODE_SLTIU:
		begin
			regwe_o = 1'b1;
			rlt_sel_o = 2'b01;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_LB:
		begin
			load_type_o = `LOAD_LB;
			regwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_LBU:
		begin
			load_type_o = `LOAD_LBU;
			regwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_LH:
		begin
			load_type_o = `LOAD_LH;
			regwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_LHU:
		begin
			load_type_o = `LOAD_LHU;
			regwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_LW:
		begin
			load_type_o = `LOAD_LW;
			regwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_LWL:
		begin
			load_type_o = `LOAD_LWL;
			regwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_LWR:
		begin
			load_type_o = `LOAD_LWR;
			regwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_SB:
		begin
			str_type_o = `STR_SB;
			memwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		`ICODE_SH:
		begin
			str_type_o = `STR_SH;
			memwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		`ICODE_SW:
		begin
			str_type_o = `STR_SW;
			memwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		`ICODE_SWL:
		begin
			str_type_o = `STR_SWL;
			memwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		`ICODE_SWR:
		begin
			str_type_o = `STR_SWR;
			memwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_LL:
		begin
			load_type_o = `LOAD_LL;
			regwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_SC:
		begin
			str_type_o = `STR_SC;
			regwe_o = 1'b1;
			rlt_sel_o = 2'b10;
			memwe_o = 1'b1;
			alusrc1_sel_o = 2'b11;
		end
		
		`ICODE_J:
		begin
			pc_sel_o = 1'b1;
			//branch_en_o = 1'b1;
			pc_branch_o = dstaddr_j;
			is_bj_inst_o = 1'b1;
		end
		`ICODE_JAL:
		begin
			regwe_o = 1'b1;
			rlt_sel_o = 2'b01;
			regdst_sel_o = 2'b10;
			pc_sel_o = 1'b1;
			//branch_en_o = 1'b1;
			pc_branch_o = dstaddr_j;
			is_bj_inst_o = 1'b1;
		end
		`ICODE_BEQ:
		begin
			is_bj_inst_o = 1'b1;
			is_bj_inst_reg_o = 1'b1;
			pc_sel_o = (rd0_i == rd1_i);
			//branch_en_o = 1'b1;
			pc_branch_o = dstaddr_b;
		end
		`ICODE_BGTZ:
		begin
			is_bj_inst_o = 1'b1;
			is_bj_inst_reg_o = 1'b1;
			pc_sel_o = ((~rd0_i[31]) && (rd0_i != 32'd0));
			//branch_en_o = 1'b1;
			pc_branch_o = dstaddr_b;
		end
		`ICODE_BLEZ:
		begin
			is_bj_inst_o = 1'b1;
			is_bj_inst_reg_o = 1'b1;
			pc_sel_o = ((rd0_i[31]) || (rd0_i == 32'd0));
			//branch_en_o = 1'b1;
			pc_branch_o = dstaddr_b;
		end
		`ICODE_BNE:
		begin
			is_bj_inst_o = 1'b1;
			is_bj_inst_reg_o = 1'b1;
			pc_sel_o = (rd0_i != rd1_i);
			//branch_en_o = 1'b1;
			pc_branch_o = dstaddr_b;
		end
		
		
		
		
		
		
		`ICODE_COP0:
			case(cpcode)
				`CPCODE_MTC:
				begin
					cp0_wen_o = 1'b1;
					regdst_sel_o = 2'b01;
					rlt_sel_o = 2'b01;
				end
				`CPCODE_MFC:
				begin
					regwe_o = 1'b1;
					rlt_sel_o = 2'b01;
				end
			endcase
		
		`ICODE_REGIMM:
			case(bzcode)
			
			`BZCODE_BLTZ:
			begin
				is_bj_inst_o = 1'b1;
				is_bj_inst_reg_o = 1'b1;
				pc_sel_o = (rd0_i[31]);
				//branch_en_o = 1'b1;
				pc_branch_o = dstaddr_b;
			end
			
			`BZCODE_BGEZ:
			begin
				is_bj_inst_o = 1'b1;
				is_bj_inst_reg_o = 1'b1;
				pc_sel_o = (~rd0_i[31]);
				//branch_en_o = 1'b1;
				pc_branch_o = dstaddr_b;
			end
			
			`BZCODE_BLTZAL:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b10;
				is_bj_inst_o = 1'b1;
				is_bj_inst_reg_o = 1'b1;
				pc_sel_o = (rd0_i[31]);
				//branch_en_o = 1'b1;
				pc_branch_o = dstaddr_b;
			end
			
			`BZCODE_BGEZAL:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b10;
				is_bj_inst_o = 1'b1;
				is_bj_inst_reg_o = 1'b1;
				pc_sel_o = (~rd0_i[31]);
				//branch_en_o = 1'b1;
				pc_branch_o = dstaddr_b;
			end
			
			`BZCODE_TEQI,
			`BZCODE_TGEI,
			`BZCODE_TGEIU,
			`BZCODE_TLTI,
			`BZCODE_TLTIU,
			`BZCODE_TNEI:
				alusrc1_sel_o = 2'b11;
			
			
			endcase
		
		
		`ICODE_SPECIAL2:
			case(opcode_i)
			
			`OPCODE_CLZ,
			`OPCODE_CLO:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_MUL:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_MADD,
			`OPCODE_MADDU,
			`OPCODE_MSUB,
			`OPCODE_MSUBU:
			begin
				hisrc_sel_o = 1'b1;
				losrc_sel_o = 1'b1;
				hiwe_o = 1'b1;
				lowe_o = 1'b1;
			end
			
			
			
			endcase
		
		
		// R指令
		`ICODE_SPECIAL:
			case(opcode_i)
			
			//逻辑类R指令
			`OPCODE_AND,
			`OPCODE_OR,
			`OPCODE_XOR,
			`OPCODE_NOR:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_SLL,
			`OPCODE_SRL,
			`OPCODE_SRA:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				alusrc0_sel_o = 1'b1;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_SLLV,
			`OPCODE_SRLV,
			`OPCODE_SRAV:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_SYNC:
			begin
				// 什么也不做
			end
			
			`OPCODE_MOVN,
			`OPCODE_MOVZ:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_MFHI:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				alusrc1_sel_o = 2'b01;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_MFLO:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				alusrc1_sel_o = 2'b10;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_MTHI:
			begin
				hiwe_o = 1'b1;
				rlt_sel_o = 2'b01;
			end
			
			`OPCODE_MTLO:
			begin
				lowe_o = 1'b1;
				rlt_sel_o = 2'b01;
			end
			
			`OPCODE_ADD,
			`OPCODE_ADDU,
			`OPCODE_SUB,
			`OPCODE_SUBU,
			`OPCODE_SLT,
			`OPCODE_SLTU:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b01;
			end
			
			`OPCODE_MULT,
			`OPCODE_MULTU:
			begin
				hisrc_sel_o = 1'b1;
				losrc_sel_o = 1'b1;
				hiwe_o = 1'b1;
				lowe_o = 1'b1;
			end
			
			`OPCODE_DIV:
			begin
				is_div_o = 1'b1;
				is_sign_div_o = 1'b1;
				hisrc_sel_o = 1'b1;
				losrc_sel_o = 1'b1;
				hiwe_o = 1'b1;
				lowe_o = 1'b1;
			end
			
			`OPCODE_DIVU:
			begin
				is_div_o = 1'b1;
				hisrc_sel_o = 1'b1;
				losrc_sel_o = 1'b1;
				hiwe_o = 1'b1;
				lowe_o = 1'b1;
			end
			
			`OPCODE_JR:
			begin
				pc_sel_o = 1'b1;
				//branch_en_o = 1'b1;
				pc_branch_o = rd0_i;
				is_bj_inst_o = 1'b1;
				is_bj_inst_reg_o = 1'b1;
			end
			`OPCODE_JALR:
			begin
				regwe_o = 1'b1;
				rlt_sel_o = 2'b01;
				regdst_sel_o = 2'b01;
				pc_sel_o = 1'b1;
				//branch_en_o = 1'b1;
				pc_branch_o = rd0_i;
				is_bj_inst_o = 1'b1;
				is_bj_inst_reg_o = 1'b1;
			end
			
			`OPCODE_TEQ,
			`OPCODE_TGE,
			`OPCODE_TGEU,
			`OPCODE_TLT,
			`OPCODE_TLTU,
			`OPCODE_TNE:
				;	
			
			
			endcase
		
		endcase
	end
end



endmodule
