module hilo
(
	input[31:0] hi_i,
	input[31:0] lo_i,
	input hi_we,
	input lo_we,
	
	output reg[31:0] hi_o,
	output reg[31:0] lo_o,
	
	input clk,
	input rst_n
);

reg[31:0] hi;
reg[31:0] lo;

always @(*)
begin
	if(!rst_n)
		hi_o <= 32'b0;
	else if(hi_we)
		hi_o <= hi_i;
	else
		hi_o <= hi;
end


always @(*)
begin
	if(!rst_n)
		lo_o <= 32'b0;
	else if(lo_we)
		lo_o <= lo_i;
	else
		lo_o <= lo;
end

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
		hi <= 32'b0;
	else if(hi_we)
		hi <= hi_i;
end

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
		lo <= 32'b0;
	else if(lo_we)
		lo <= lo_i;
end


endmodule
