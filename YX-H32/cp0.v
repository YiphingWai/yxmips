`include "./defines.v"
module cp0
(
	input we_i,
	input[4:0] wa_i,
	input[4:0] ra_i,
	input[31:0] wd_i,
	
	input[7:0] except_type_i,
	input[4:0] int_i,
	input[31:0] vic_inst_addr_i,
	input is_in_delayslot_i,
	input[31:0] badvaddr_i,
	
	output reg[31:0] data_o,
	output reg[31:0] count_o,
	output reg[31:0] compare_o,
	output reg[31:0] status_o,
	output reg[31:0] cause_o,
	output reg[31:0] epc_o,
	output reg[31:0] config_o,
	output reg[31:0] prid_o,
	output reg[31:0] badvaddr_o,
	
	//组合逻辑输出
	output reg exc_en_o,
	output reg[31:0] exc_addr_o,
	
	input	clk,
	input rst_n
);

reg timer_int;
//reg[31:0] count_new;
//reg[31:0] compare_new;
//reg[31:0] status_new;
reg[31:0] epc_new;
reg[31:0] config_new;
reg[31:0] prid_new; 

wire[5:0] irq = status_o[1] ? 6'b0 : {int_i, timer_int} & status_o[15:10];	//屏蔽EXL=1时的外部中断
wire[2:0] swirq = status_o[1] ? 2'b0 : cause_o[9:8] & status_o[9:8];	//屏蔽EXL=1时的软中断
wire[4:0] cause = except_type_i[6] ? 5'd4 : except_type_i[7] ? 5'd5 :except_type_i[1] ? 5'd8 : except_type_i[0] ? 5'd9 :except_type_i[2] ? 5'd10 : except_type_i[3] ? 5'd12 : except_type_i[4] ? 5'd13 : except_type_i[5] ? 5'd31 : ((irq!=6'd0) || (swirq!=2'd0)) ? 5'd0 : 5'd30;

always @(posedge clk, negedge rst_n)
	if(!rst_n)
	begin
		count_o <= 32'b0;
		compare_o <= 32'b0;
		timer_int <= 1'b0;
		status_o <= 32'b00010000000000000000000000000000;
		cause_o <= 32'b0;
		epc_o <= 32'b0;
		prid_o <= {8'b0, 8'h57, 10'h4, 6'h2};
		config_o <= 32'b0;
		badvaddr_o<=32'd0;
	end
	else 
	begin
		prid_o <= {8'b0, 8'h57, 10'h4, 6'h2};
		config_o <= 32'b0;
		count_o <= count_o + 32'b1;
		if((count_o == compare_o) && (count_o != 32'd0))
			timer_int <= 1'b1;
		
		if(except_type_i != 8'd0)
		begin
			if(cause == 5'd31)
			begin
				status_o[1] <= 1'b0;
			end
			else if(!status_o[1])
			begin
				if (cause==5'd4)
				begin
					if(vic_inst_addr_i[1:0]!=2'd0)
						badvaddr_o<=vic_inst_addr_i;
					else
						badvaddr_o<=badvaddr_i;
				end
				else if(cause==5'd5)
				begin
					badvaddr_o<=badvaddr_i;
				end
				
				if(is_in_delayslot_i)
				begin
					epc_o <= vic_inst_addr_i - 32'd4;
					cause_o[31] <= 1'b1;
				end
				else
				begin
					epc_o <= vic_inst_addr_i;
					cause_o[31] <= 1'b0;
				end
				status_o[1] <= 1'b1;
				cause_o[6:2] <= cause;
				cause_o[15:10] <= irq;
			end
			else
			begin
				cause_o[6:2] <= cause;
				cause_o[15:10] <= irq;
			end
		end
		else if((irq != 6'd0) && status_o[0])	//中断发生（EXL=1时的外部中断自动屏蔽了）
		begin
			if(!status_o[1])
			begin
				if(is_in_delayslot_i)
				begin
					epc_o <= vic_inst_addr_i - 32'd4;
					cause_o[31] <= 1'b1;
				end
				else
				begin
					epc_o <= vic_inst_addr_i;
					cause_o[31] <= 1'b0;
				end
				status_o[1] <= 1'b1;
				cause_o[6:2] <= cause;
				cause_o[15:10] <= irq;
			end
			else
			begin
				cause_o[6:2] <= cause;
				cause_o[15:10] <= irq;
			end
		end
		else if((swirq != 2'd0) && status_o[0])	//软中断发生（EXL=1时的中断自动屏蔽了）
		begin
			if(!status_o[1])
			begin
				if(is_in_delayslot_i)
				begin
					epc_o <= vic_inst_addr_i - 32'd4;
					cause_o[31] <= 1'b1;
				end
				else
				begin
					epc_o <= vic_inst_addr_i;
					cause_o[31] <= 1'b0;
				end
				status_o[1] <= 1'b1;
				cause_o[6:2] <= cause;
				cause_o[15:10] <= irq;
			end
			else
			begin
				cause_o[6:2] <= cause;
				cause_o[15:10] <= irq;
			end
		end
		
		
		
		if(we_i)
		begin
			case(wa_i)
			
			`CP0_COUNT:
				count_o <= wd_i;
			`CP0_COMPARE:
			begin
				compare_o <= wd_i;
				timer_int <= 1'b0;
			end
			`CP0_STATUS:
				status_o <= wd_i;
			`CP0_CAUSE:
			begin
				cause_o[9:8] <= wd_i[9:8];
				cause_o[23] <= wd_i[23];
				cause_o[22] <= wd_i[22];
			end
			`CP0_EPC:
				epc_o <= wd_i;
			endcase
		end
		
	end
		

always @(*)
	if(we_i)
	begin
//		count_new = count_o;
//		compare_new = compare_o;
//		status_new = status_o;
		epc_new = epc_o;
		case(wa_i)
		
//		`CP0_COUNT:	
//			count_new = wd_i;
//		`CP0_COMPARE:
//			compare_new = wd_i;
//		`CP0_STATUS:
//			status_new = wd_i;
		`CP0_EPC:
			epc_new = wd_i;
		endcase
	end
	else
	begin
//		count_new = count_o;
//		compare_new = compare_o;
//		status_new = status_o;
		epc_new = epc_o;
	end
	
always @(*)
	if(!rst_n) 
		data_o = 32'd0;
	else if(we_i && (wa_i == ra_i))
		data_o = wd_i;
	else
		case (ra_i) 
		`CP0_BADVADDR:		
		begin
			data_o = badvaddr_o ;
		end
		`CP0_COUNT:		
		begin
			data_o = count_o ;
		end
		`CP0_COMPARE:	
		begin
			data_o = compare_o ;
		end
		`CP0_STATUS:	
		begin
			data_o = status_o ;
		end
		`CP0_CAUSE:	
		begin
			data_o = {cause_o[31:16], {int_i, timer_int}, cause_o[9:0]};
		end
		`CP0_EPC:	
		begin
			data_o = epc_o ;
		end
		`CP0_PRID:	
		begin
			data_o = prid_o ;
		end
		`CP0_CONFIG:	
		begin
			data_o = config_o ;
		end	
		default: 	
			data_o = 32'd0;			
		endcase

//exc judge
always @(*)
	if(!rst_n)
	begin
		exc_addr_o = 32'b0;
		exc_en_o = 1'b0;
	end
	else if(except_type_i != 8'd0)	//中断发生（EXL=1时的外部中断自动屏蔽了）
	begin
		exc_en_o = 1'b1;
		//中断向量
		case(cause)
		
//		5'd0:						//外部中断
//			exc_addr_o = 32'h30;
		5'd4:                       //加载或取值地址不对齐
			exc_addr_o = 32'hbfc00380;
		5'd5:						//存储地址不对齐
			exc_addr_o = 32'hbfc00380;
		5'd8:						//syscall
			exc_addr_o = 32'hbfc00380;
		5'd9:						//break
			exc_addr_o = 32'hbfc00380;
		5'd10:					//unused inst
			exc_addr_o = 32'hbfc00380;
		5'd12:					//overflow
			exc_addr_o = 32'hbfc00380;
		5'd13:					//自陷
			exc_addr_o = 32'hbfc00380;
		5'd31:					//eret 考虑特殊情况：前条指令在W级写回epc，eret指令在M级将得到旧值
			exc_addr_o = epc_new;
			
		default:
		begin
			exc_en_o = 1'b0;
			exc_addr_o = 32'b0;
		end
		endcase
	end
	else if((irq != 6'd0) && status_o[0])	//中断发生（EXL=1时的外部中断自动屏蔽了）
	begin
		exc_en_o = 1'b1;
		//中断向量
		if(cause == 5'd0)
			exc_addr_o = 32'h30;
		else
		begin
			exc_en_o = 1'b0;
			exc_addr_o = 32'b0;
		end
	end
	else if((swirq != 2'd0) && status_o[0])	//中断发生（EXL=1时的外部中断自动屏蔽了）
	begin
		exc_en_o = 1'b1;
		//中断向量
		if(cause == 5'd0)
			exc_addr_o = 32'h30;
		else
		begin
			exc_en_o = 1'b0;
			exc_addr_o = 32'b0;
		end
	end
	else
	begin
		exc_en_o = 1'b0;
		exc_addr_o = 32'b0;
	end
endmodule
