module signimm
(
	input[15:0] imm_i,
	input sign_i,		//��/�з�����չѡ��
	
	output[31:0] signimm_o
);

assign signimm_o = sign_i ? {{16{imm_i[15]}}, imm_i[15:0]} : {16'b0, imm_i[15:0]};

endmodule
