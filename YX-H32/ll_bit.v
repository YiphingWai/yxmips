module ll_bit
(
	input ll_bit_i,
	input we_i,
	
	input clr_i,
	
	output reg ll_bit_o,
	
	input clk,
	input rst_n
);

reg ll_bit;

always @(*)
begin
	if(!rst_n)
		ll_bit_o = 1'b0;
	else if(clr_i)
		ll_bit_o = 1'b0;
	else if(we_i)
		ll_bit_o = ll_bit_i;
	else
		ll_bit_o = ll_bit;
end

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
		ll_bit <= 1'b0;
	else if(clr_i)
		ll_bit <= 1'b0;
	else if(we_i)
		ll_bit <= ll_bit_i;
end

endmodule
