module syncD2E
(
	input is_div_i,
	input is_sign_div_i,
	input[7:0] exc_mask_i,
	input is_delayslot_i,
	input cp0_wen_i,
	input[3:0] str_type_i,
	input[3:0] load_type_i,
	input hisrc_sel_i,
	input losrc_sel_i,
	input hiwe_i,
	input lowe_i,
	input regwe_i,
	input[1:0] rlt_sel_i,
	input memwe_i,
	input[7:0] alucode_i,
	input alusrc0_sel_i,
	input[1:0] alusrc1_sel_i,
	input[1:0] regdst_sel_i,
	
	input[31:0] rd0_i,
	input[31:0] rd1_i,
	
	input[31:0] hi_i,
	input[31:0] lo_i,
	
	input[31:0] cp0_i,
	
	input[4:0] rs_i,
	input[4:0] rt_i,
	input[4:0] rd_i,
	
	input[31:0] signimm_i,
	input[31:0] pc_plus4_i,
	input[7:0]	fetch_exc_i,
//--------------------------------------
	output reg is_div_o,
	output reg is_sign_div_o,
	output reg[7:0] exc_mask_o,
	output reg is_delayslot_o,
	output reg cp0_wen_o,
	output reg[3:0] str_type_o,
	output reg[3:0] load_type_o,
	output reg hisrc_sel_o,
	output reg losrc_sel_o,
	output reg hiwe_o,
	output reg lowe_o,
	output reg regwe_o,
	output reg[1:0] rlt_sel_o,
	output reg memwe_o,
	output reg[7:0] alucode_o,
	output reg alusrc0_sel_o,
	output reg[1:0] alusrc1_sel_o,
	output reg[1:0] regdst_sel_o,
	
	output reg[31:0] rd0_o,
	output reg[31:0] rd1_o,
	
	output reg[31:0] hi_o,
	output reg[31:0] lo_o,
	
	output reg[31:0] cp0_o,
	
	output reg[4:0] rs_o,
	output reg[4:0] rt_o,
	output reg[4:0] rd_o,
	
	output reg[31:0] signimm_o,
	
	output reg[31:0] pc_plus4_o,
	output reg[7:0]	 fetch_exc_o,
	
	input irq_i,
	
	input clr0_i,
	input clr1_i,
	input clr2_i,
	
	input stall0_i,
	
	input rst_n,
	input clk
);

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
	begin
		is_div_o <= 1'b0;
		is_sign_div_o <= 1'b0;
		exc_mask_o <= 8'd0;
		is_delayslot_o <= 1'b0;
		cp0_wen_o <= 1'b0;
		str_type_o <= 4'b0;
		load_type_o <= 4'b0;
		hisrc_sel_o <= 1'b0;
		losrc_sel_o <= 1'b0;
		hiwe_o <= 1'b0;
		lowe_o <= 1'b0;
		regwe_o <= 1'b0;
		rlt_sel_o <= 2'b0;
		memwe_o <= 1'b0;
		alucode_o <= 8'b0;
		alusrc0_sel_o <= 1'b0;
		alusrc1_sel_o <= 2'b0;
		regdst_sel_o <= 2'b0;
			
		rd0_o <= 32'd0;
		rd1_o <= 32'd0;
			
		hi_o <= 32'd0;
		lo_o <= 32'd0;
		
		cp0_o <= 32'd0;
			
		rs_o <= 5'd0;
		rt_o <= 5'd0;
		rd_o <= 5'd0;
			
		signimm_o <= 32'd0;
		pc_plus4_o <= 32'd0;
		fetch_exc_o<=8'd0;
	end
	else if(irq_i)
	begin
		is_div_o <= 1'b0;
		is_sign_div_o <= 1'b0;
		exc_mask_o <= 8'd0;
		is_delayslot_o <= 1'b0;
		cp0_wen_o <= 1'b0;
		str_type_o <= 4'b0;
		load_type_o <= 4'b0;
		hisrc_sel_o <= 1'b0;
		losrc_sel_o <= 1'b0;
		hiwe_o <= 1'b0;
		lowe_o <= 1'b0;
		regwe_o <= 1'b0;
		rlt_sel_o <= 2'b0;
		memwe_o <= 1'b0;
		alucode_o <= 8'b0;
		alusrc0_sel_o <= 1'b0;
		alusrc1_sel_o <= 2'b0;
		regdst_sel_o <= 2'b0;
			
		rd0_o <= 32'd0;
		rd1_o <= 32'd0;
			
		hi_o <= 32'd0;
		lo_o <= 32'd0;
		
		cp0_o <= 32'd0;
			
		rs_o <= 5'd0;
		rt_o <= 5'd0;
		rd_o <= 5'd0;
			
		signimm_o <= 32'd0;
		pc_plus4_o <= 32'd0;
		fetch_exc_o<=8'd0;
	end
	else if(stall0_i)
	begin
		//do nothing
	end
	else if(clr0_i || clr1_i || clr2_i)
	begin
		is_div_o <= 1'b0;
		is_sign_div_o <= 1'b0;
		exc_mask_o <= 8'd0;
		is_delayslot_o <= 1'b0;
		cp0_wen_o <= 1'b0;
		str_type_o <= 4'b0;
		load_type_o <= 4'b0;
		hisrc_sel_o <= 1'b0;
		losrc_sel_o <= 1'b0;
		hiwe_o <= 1'b0;
		lowe_o <= 1'b0;
		regwe_o <= 1'b0;
		rlt_sel_o <= 2'b0;
		memwe_o <= 1'b0;
		alucode_o <= 8'b0;
		alusrc0_sel_o <= 1'b0;
		alusrc1_sel_o <= 2'b0;
		regdst_sel_o <= 2'b0;
			
		rd0_o <= 32'd0;
		rd1_o <= 32'd0;
			
		hi_o <= 32'd0;
		lo_o <= 32'd0;
		
		cp0_o <= 32'd0;
			
		rs_o <= 5'd0;
		rt_o <= 5'd0;
		rd_o <= 5'd0;
			
		signimm_o <= 32'd0;
		pc_plus4_o <= 32'd0;
		fetch_exc_o<=8'd0;
	end
	else
	begin
		is_div_o <= is_div_i;
		is_sign_div_o <= is_sign_div_i;
		exc_mask_o <= exc_mask_i;
		is_delayslot_o <= is_delayslot_i;
		cp0_wen_o <= cp0_wen_i;
		str_type_o <= str_type_i;
		load_type_o <= load_type_i;
		hisrc_sel_o <= hisrc_sel_i;
		losrc_sel_o <= losrc_sel_i;
		hiwe_o <= hiwe_i;
		lowe_o <= lowe_i;
		regwe_o <= regwe_i;
		rlt_sel_o <= rlt_sel_i;
		memwe_o <= memwe_i;
		alucode_o <= alucode_i;
		alusrc0_sel_o <= alusrc0_sel_i;
		alusrc1_sel_o <= alusrc1_sel_i;
		regdst_sel_o <= regdst_sel_i;
			
		rd0_o <= rd0_i;
		rd1_o <= rd1_i;
			
		hi_o <= hi_i;
		lo_o <= lo_i;
		
		cp0_o <= cp0_i;
			
		rs_o <= rs_i;
		rt_o <= rt_i;
		rd_o <= rd_i;
			
		signimm_o <= signimm_i;
		pc_plus4_o <= pc_plus4_i;
		fetch_exc_o<=fetch_exc_i;
	end
	
end


endmodule
