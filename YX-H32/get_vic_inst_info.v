module get_vic_inst_info
(
	input[31:0] pc_i_F,	
	input[31:0] pc_plus4_i_D,
	input[31:0] pc_plus4_i_E,
	input[31:0] pc_plus4_i_M,
	
	input is_delayslot_i_D,
	input is_delayslot_i_E,
	input is_delayslot_i_M,
	
	output reg vic_is_delayslot_o,
	output reg[31:0] vic_addr_o
);

always @(*)
begin
	if(pc_plus4_i_M != 32'b0)
	begin
		vic_addr_o = pc_plus4_i_M - 32'd4;
		vic_is_delayslot_o = is_delayslot_i_M;
	end
	else if(pc_plus4_i_E != 32'b0)
	begin
		vic_addr_o = pc_plus4_i_E - 32'd4;
		vic_is_delayslot_o = is_delayslot_i_E;
	end
	else if(pc_plus4_i_D != 32'b0)
	begin
		vic_addr_o = pc_plus4_i_D - 32'd4;
		vic_is_delayslot_o = is_delayslot_i_D;
	end
	else		//针对中断信号持续有效的情况。此时受害指令刚刚从epc回到pc，且此指令必非延迟槽
	begin
		vic_addr_o = pc_i_F;
		vic_is_delayslot_o = 1'b0;
	end
end


endmodule
