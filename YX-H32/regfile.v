module regfile
(
	input[4:0] ra0_i,
	input[4:0] ra1_i,
	
	input[4:0] wa0_i,
	input[31:0] wd0_i,
	input we0,
	
	output reg[31:0] rd0_o,
	output reg[31:0] rd1_o,
	
	output[31:0] reg1_o,
	
	input rst_n,
	input clk
	
);

reg[31:0]regs[31:0];

assign reg1_o = regs[1];

always @(*)
begin
	if(!rst_n)
		rd0_o = 32'd0;
	else if(ra0_i == 5'd0)
		rd0_o = 32'd0;
	else if((ra0_i == wa0_i) && we0)
		rd0_o = wd0_i;
	else
		rd0_o = regs[ra0_i];
end

always @(*)
begin
	if(!rst_n)
		rd1_o = 32'd0;
	else if(ra1_i == 5'd0)
		rd1_o = 32'd0;
	else if((ra1_i == wa0_i) && we0)
		rd1_o = wd0_i;
	else
		rd1_o = regs[ra1_i];
end

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
		;//do nothing
	else
		if(we0 && (wa0_i != 5'd0))
			regs[wa0_i] <= wd0_i;
	
end


endmodule
