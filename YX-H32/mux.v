module mux_41
(
	input[31:0] in0_i,
	input[31:0] in1_i,
	input[31:0] in2_i,
	input[31:0] in3_i,
	
	input[1:0] sel_i,
	
	output[31:0] out_o
); 

assign out_o = (sel_i==2'd0) ? in0_i : (sel_i==2'd1) ? in1_i : (sel_i==2'd2) ? in2_i : in3_i;


endmodule

module mux_31
(
	input[31:0] in0_i,
	input[31:0] in1_i,
	input[31:0] in2_i,
	
	input[1:0] sel_i,
	
	output[31:0] out_o
);

assign out_o = (sel_i==2'd1) ? in1_i : (sel_i==2'd2) ? in2_i : in0_i;

endmodule

module mux_21
(
	input[31:0] in0_i,
	input[31:0] in1_i,
	
	input sel_i,
	
	output[31:0] out_o
); 

assign out_o = sel_i ? in1_i : in0_i;

endmodule


module mux5b_31
(
	input[4:0] in0_i,
	input[4:0] in1_i,
	input[4:0] in2_i,
	
	input[1:0] sel_i,
	
	output[4:0] out_o
); 

assign out_o = (sel_i==2'd1) ? in1_i : (sel_i==2'd2) ? in2_i : in0_i;

endmodule

