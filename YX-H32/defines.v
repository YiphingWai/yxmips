`define ICODE_SPECIAL	6'b000000
`define ICODE_SPECIAL2	6'b011100
`define ICODE_ANDI		6'b001100
`define ICODE_XORI		6'b001110
`define ICODE_LUI			6'b001111
`define ICODE_ORI			6'b001101
`define ICODE_PREF		6'b110011
`define ICODE_ADDI		6'b001000
`define ICODE_ADDIU		6'b001001
`define ICODE_SLTI		6'b001010
`define ICODE_SLTIU		6'b001011

`define ICODE_LB			6'b100000
`define ICODE_LBU			6'b100100
`define ICODE_LH			6'b100001
`define ICODE_LHU			6'b100101
`define ICODE_LW			6'b100011
`define ICODE_LWL			6'b100010
`define ICODE_LWR			6'b100110
`define ICODE_SB			6'b101000
`define ICODE_SH			6'b101001
`define ICODE_SW			6'b101011
`define ICODE_SWL			6'b101010
`define ICODE_SWR			6'b101110
`define ICODE_LL			6'b110000
`define ICODE_SC			6'b111000

`define ICODE_J			6'b000010
`define ICODE_JAL			6'b000011
`define ICODE_BEQ			6'b000100
`define ICODE_BGTZ		6'b000111
`define ICODE_BLEZ		6'b000110
`define ICODE_BNE			6'b000101

`define ICODE_REGIMM		6'b000001

`define ICODE_COP0		6'b010000

//--------------------------------------------------------
`define CPCODE_MTC		5'b00100
`define CPCODE_MFC		5'b00000
`define INST_ERET 		32'b01000010000000000000000000011000
//--------------------------------------------------------
`define BZCODE_BLTZ		5'b00000
`define BZCODE_BLTZAL	5'b10000
`define BZCODE_BGEZ		5'b00001
`define BZCODE_BGEZAL	5'b10001

`define BZCODE_TEQI		5'b01100
`define BZCODE_TGEI		5'b01000
`define BZCODE_TGEIU		5'b01001
`define BZCODE_TLTI		5'b01010
`define BZCODE_TLTIU		5'b01011
`define BZCODE_TNEI		5'b01110
//--------------------------------------------------------
`define OPCODE_AND		6'b100100
`define OPCODE_OR			6'b100101
`define OPCODE_XOR		6'b100110
`define OPCODE_NOR		6'b100111

`define OPCODE_SLL		6'b000000
`define OPCODE_SRL		6'b000010
`define OPCODE_SRA		6'b000011
`define OPCODE_SLLV		6'b000100
`define OPCODE_SRLV		6'b000110
`define OPCODE_SRAV		6'b000111

`define OPCODE_SYNC		6'b001111

`define OPCODE_MOVN		6'b001011
`define OPCODE_MOVZ		6'b001010
`define OPCODE_MFHI		6'b010000
`define OPCODE_MFLO		6'b010010
`define OPCODE_MTHI		6'b010001
`define OPCODE_MTLO		6'b010011

`define OPCODE_ADD		6'b100000
`define OPCODE_ADDU		6'b100001
`define OPCODE_SUB		6'b100010
`define OPCODE_SUBU		6'b100011
`define OPCODE_SLT		6'b101010
`define OPCODE_SLTU		6'b101011

`define OPCODE_CLZ		6'b100000
`define OPCODE_CLO		6'b100001

`define OPCODE_MUL		6'b000010
`define OPCODE_MULT		6'b011000
`define OPCODE_MULTU		6'b011001

`define OPCODE_JR			6'b001000
`define OPCODE_JALR		6'b001001

`define OPCODE_MADD		6'b000000
`define OPCODE_MADDU		6'b000001
`define OPCODE_MSUB		6'b000100
`define OPCODE_MSUBU		6'b000101

`define OPCODE_DIV		6'b011010
`define OPCODE_DIVU		6'b011011

`define OPCODE_TEQ		6'b110100
`define OPCODE_TGE		6'b110000
`define OPCODE_TGEU		6'b110001
`define OPCODE_TLT		6'b110010
`define OPCODE_TLTU		6'b110011
`define OPCODE_TNE		6'b110110

`define OPCODE_SYSCALL	6'b001100
`define OPCODE_BREAK	6'b001101
//--------------------------------------------------------

`define ALUOP_NOP		8'd0

`define ALUOP_AND		8'd1
`define ALUOP_OR		8'd2
`define ALUOP_XOR		8'd3
`define ALUOP_NOR		8'd4
`define ALUOP_LU		8'd5
`define ALUOP_ANDI	8'd6
`define ALUOP_XORI	8'd7
`define ALUOP_ORI		8'd8
 
`define ALUOP_SLL		8'd9
`define ALUOP_SRL		8'd10
`define ALUOP_SRA		8'd11
`define ALUOP_SLLV	8'd12
`define ALUOP_SRLV	8'd13
`define ALUOP_SRAV	8'd14

`define ALUOP_MOVN	8'd15
`define ALUOP_MOVZ	8'd16
`define ALUOP_MFHI	8'd17
`define ALUOP_MFLO	8'd18
`define ALUOP_MTHI	8'd19
`define ALUOP_MTLO	8'd20

`define ALUOP_ADD		8'd21
`define ALUOP_ADDU	8'd22
`define ALUOP_SUB		8'd23
`define ALUOP_SUBU	8'd24
`define ALUOP_SLT		8'd25
`define ALUOP_SLTU	8'd26
`define ALUOP_ADDI	8'd27
`define ALUOP_ADDIU	8'd28
`define ALUOP_SLTI	8'd29
`define ALUOP_SLTIU	8'd30
`define ALUOP_CLZ		8'd31
`define ALUOP_CLO		8'd32
`define ALUOP_MUL		8'd33
`define ALUOP_MULT	8'd34
`define ALUOP_MULTU	8'd35
`define ALUOP_LB		8'd36
`define ALUOP_LBU		8'd37
`define ALUOP_LH		8'd38
`define ALUOP_LHU		8'd39
`define ALUOP_LW		8'd40
`define ALUOP_LWL		8'd41
`define ALUOP_LWR		8'd42
`define ALUOP_SB		8'd43
`define ALUOP_SH		8'd44
`define ALUOP_SW		8'd45
`define ALUOP_SWL		8'd46
`define ALUOP_SWR		8'd47
`define ALUOP_LL		8'd48
`define ALUOP_SC		8'd49
`define ALUOP_JALR	8'd50
`define ALUOP_MADD	8'd51
`define ALUOP_MADDU	8'd52
`define ALUOP_MSUB	8'd53		
`define ALUOP_MSUBU	8'd54
`define ALUOP_MTC		8'd55
`define ALUOP_MFC		8'd56

`define ALUOP_TEQ		8'd57
`define ALUOP_TGE		8'd58
`define ALUOP_TGEU	8'd59
`define ALUOP_TLT		8'd60
`define ALUOP_TLTU	8'd61
`define ALUOP_TNE		8'd62
//--------------------------------------------------------


`define LOAD_LB			4'd1	//必须从1开始。防冲突模块据此判断是否正在执行LOAD类指令
`define LOAD_LBU			4'd2
`define LOAD_LH			4'd3
`define LOAD_LHU			4'd4
`define LOAD_LW			4'd5
`define LOAD_LWL			4'd6
`define LOAD_LWR			4'd7
`define LOAD_LL			4'd8

`define STR_SB				4'd1
`define STR_SH				4'd2
`define STR_SW				4'd3
`define STR_SWL			4'd4
`define STR_SWR			4'd5
`define STR_SC				4'd6


//--------------------------------------------------------

`define CP0_BADVADDR 5'b01000
`define CP0_COUNT    5'b01001        
`define CP0_COMPARE    5'b01011     
`define CP0_STATUS    5'b01100     
`define CP0_CAUSE    5'b01101       
`define CP0_EPC    5'b01110          
`define CP0_PRID    5'b01111       
`define CP0_CONFIG    5'b10000   
