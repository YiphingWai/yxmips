module ir
(
	input[31:0] ir_i,
	input[31:0] pc_plus4_i,
	input is_bj_inst_i,
	input[7:0]	fetch_exc_i,
	
	output reg[31:0] ir_o,
	output reg[31:0] pc_plus4_o,
	output reg is_bj_inst_o,
	output reg[7:0]  fetch_exc_o,
	
	input stall0_i,
	input stall1_i,
	input stall2_i,
	input stall3_i,
	input irq_i,
	input rst_n,
	input clk
);

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
	begin
		ir_o <= 32'd0;
		pc_plus4_o <= 32'd0;
		is_bj_inst_o <= 1'b0;
		fetch_exc_o	<=8'd0;
	end
	else if(irq_i)
	begin
		ir_o <= 32'd0;
		pc_plus4_o <= 32'd0;
		is_bj_inst_o <= 1'b0;
		fetch_exc_o	<=8'd0;
	end
	else if((!stall0_i) && (!stall1_i) && (!stall2_i) && (!stall3_i))
	begin
		ir_o <= ir_i;
		pc_plus4_o <= pc_plus4_i;
		is_bj_inst_o <= is_bj_inst_i;
		fetch_exc_o	<=	fetch_exc_i;
	end
end


endmodule
