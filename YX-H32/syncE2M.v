module syncE2M
(
	input[7:0] exc_mask_i,
	input is_delayslot_i,
	input[4:0] int_i,
	input cp0_wen_i,
	input[3:0] str_type_i,
	input[3:0] load_type_i,
	input hisrc_sel_i,
	input losrc_sel_i,
	input hiwe_i,
	input lowe_i,
	input regwe_i,
	input[1:0] rlt_sel_i,
	input memwe_i,
	
	input[31:0] regdat0_i,
	input[31:0] regdat1_i,
	input[31:0] aluout_i,
	input sc_rlt_i,
	input[3:0] byte_vld_i,
	input[63:0] mulrlt_i,
	
	input[4:0] regdst_i,
	
	input[31:0] pc_plus4_i,
//-----------------------------------
	output reg[7:0] exc_mask_o,
	output reg is_delayslot_o,
	output reg[4:0] int_o,
	output reg cp0_wen_o,
	output reg[3:0] str_type_o,
	output reg[3:0] load_type_o,
	output reg hisrc_sel_o,
	output reg losrc_sel_o,
	output reg hiwe_o,
	output reg lowe_o,
	output reg regwe_o,
	output reg[1:0] rlt_sel_o,
	output reg memwe_o,
	
	output reg[31:0] regdat0_o,
	output reg[31:0] regdat1_o,
	output reg[31:0] aluout_o,
	output reg sc_rlt_o,
	output reg[3:0] byte_vld_o,
	output reg[63:0] mulrlt_o,
	
	output reg[4:0] regdst_o,
	
	output reg[31:0] pc_plus4_o,
	
	input irq_i,
	input stall0_i,
	input stall1_i,
	input clr_i,
	
	input clk,
	input rst_n
	
);

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
	begin
		exc_mask_o <= 8'd0;
		is_delayslot_o <= 1'b0;
		int_o <= 5'b0;
		cp0_wen_o <= 1'b0;
		str_type_o <= 4'b0;
		load_type_o <= 4'b0;
		hisrc_sel_o <= 1'b0;
		losrc_sel_o <= 1'b0;
		hiwe_o <= 1'b0;
		lowe_o <= 1'b0;
		regwe_o <= 1'b0;
		rlt_sel_o <= 2'b0;
		memwe_o <= 1'b0;
			
		regdat0_o <= 1'b0;
		regdat1_o <= 1'b0;
		aluout_o <= 32'b0;
		sc_rlt_o <= 1'b0;
		byte_vld_o <= 4'd0;
		mulrlt_o <= 64'b0;
			
		regdst_o <= 5'b0;
		pc_plus4_o <= 32'd0;
	end
	else if(irq_i)
	begin
		exc_mask_o <= 8'd0;
		is_delayslot_o <= 1'b0;
		int_o <= 5'b0;
		cp0_wen_o <= 1'b0;
		str_type_o <= 4'b0;
		load_type_o <= 4'b0;
		hisrc_sel_o <= 1'b0;
		losrc_sel_o <= 1'b0;
		hiwe_o <= 1'b0;
		lowe_o <= 1'b0;
		regwe_o <= 1'b0;
		rlt_sel_o <= 2'b0;
		memwe_o <= 1'b0;
			
		regdat0_o <= 1'b0;
		regdat1_o <= 1'b0;
		aluout_o <= 32'b0;
		sc_rlt_o <= 1'b0;
		byte_vld_o <= 4'd0;
		mulrlt_o <= 64'b0;
			
		regdst_o <= 5'b0;
		pc_plus4_o <= 32'd0;
	end	
	else if(stall0_i || stall1_i)
	begin
		;
	end
	else if(clr_i)
	begin
		exc_mask_o <= 8'd0;
		is_delayslot_o <= 1'b0;
		int_o <= 5'b0;
		cp0_wen_o <= 1'b0;
		str_type_o <= 4'b0;
		load_type_o <= 4'b0;
		hisrc_sel_o <= 1'b0;
		losrc_sel_o <= 1'b0;
		hiwe_o <= 1'b0;
		lowe_o <= 1'b0;
		regwe_o <= 1'b0;
		rlt_sel_o <= 2'b0;
		memwe_o <= 1'b0;
			
		regdat0_o <= 1'b0;
		regdat1_o <= 1'b0;
		aluout_o <= 32'b0;
		sc_rlt_o <= 1'b0;
		byte_vld_o <= 4'd0;
		mulrlt_o <= 64'b0;
			
		regdst_o <= 5'b0;
		pc_plus4_o <= 32'd0;
	end
	else
	begin
		exc_mask_o <= exc_mask_i;
		is_delayslot_o <= is_delayslot_i;
		int_o <= int_i;
		cp0_wen_o <= cp0_wen_i;
		str_type_o <= str_type_i;
		load_type_o <= load_type_i;
		hisrc_sel_o <= hisrc_sel_i;
		losrc_sel_o <= losrc_sel_i;
		hiwe_o <= hiwe_i;
		lowe_o <= lowe_i;
		regwe_o <= regwe_i;
		rlt_sel_o <= rlt_sel_i;
		memwe_o <= memwe_i;
			
		regdat0_o <= regdat0_i;
		regdat1_o <= regdat1_i;
		aluout_o <= aluout_i;
		sc_rlt_o <= sc_rlt_i;
		byte_vld_o <= byte_vld_i;
		mulrlt_o <= mulrlt_i;
			
		regdst_o <= regdst_i;
		pc_plus4_o <= pc_plus4_i;
	end
end


endmodule
